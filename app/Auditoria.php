<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Auditoria extends Model
{
    protected $table='auditoria';
    protected $fillable=['id','accion','id_user'];

    public function usuario()
    {
    	return $this->BelongsTo('App\User','id_user');
    }
}
