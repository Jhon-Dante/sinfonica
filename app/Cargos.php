<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cargos extends Model
{
    protected $table='cargos';
    protected $fillable=['id','cargo','status'];

    public function personal()
    {
    	return $this->HasMany('App/Datos_basicos_personal','id_cargo','id');
    }
}
