<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ciudades extends Model
{
    protected $table='ciudades';
    protected $filable=['id','ciudad','capital','id_estado'];

    public function estado()
    {
    	return $this->belongsTo('App\Estados','id_estado');
    }

    public function nucleo()
    {
    	return $this->HasMany('App\Modulos','id_ciudad','id');
    }
    public function personal()
    {
        return $this->HasMany('App\Datos_basicos_personal','id_ciudad','id');
    }
}
