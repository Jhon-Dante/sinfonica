<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coordinadores extends Model
{
    protected $table='coordinadores';
    protected $fillable=['id','nombre','apellido','nacio','cedula','direccion','cod_telefono','telefono','email'];
    
}
