<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cursos extends Model
{
    protected $table='cursos';
    protected $fillable=['id','curso'];

    public function preinscripcion()
    {
    	return $this->HasMany('App\Preinscripcion','id_curso','id');
    }
}
