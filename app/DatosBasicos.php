<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DatosBasicos extends Model
{
    protected $table='datosBasicos';
    protected $fillable=['id','nombres','apellidos','nacio','cedula','lugar_nac','fecha_nac','genero','vive_p','vive_m','vive_o','direccion','cod_tel','telefono','status'];

    public function datoBasico_m()
    {
    	return $this->HasOne('App\DatosBasicos_m','id_datosBasicos');
    }

    public function datoBasico_e()
    {
    	return $this->HasOne('App\DatosBasicos_e ','id_datosBasicos');
    }

    public function datoBasico_has_representante()
    {
    	return $this->HasMany('App\DatosBasicos_has_representantes ','id_datosBasicos');
    }

    public function inscripcion()
    {
        return $this->HasMany('App\Inscripcion ','id_datosBasicos');
    }

}
