<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DatosBasicos_e extends Model
{
    protected $table='datosBasicos_e';
    protected $fillable=['id','institucion','tipo','direccion','nombre_director','grado','turno','id_datosBasicos'];


    public function datoBasico()
    {
    	return $this->belongsTo('App\DatosBasicos','id_datosBasicos');
    }
}
