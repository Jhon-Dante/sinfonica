<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DatosBasicos_has_representantes extends Model
{
    protected $table='datosBasicos_has_representantes';
    protected $fillable=['id','id_datosBasicos','id_representantes','id_parentesco'];


    public function datoBasico()
    {
    	return $this->belongsTo('App\DatosBasicos','id_datosBasicos');
    }

    public function representante()
    {
    	return $this->belongsTo('App\Representantes','id_representantes');
    }

    public function parentesco()
    {
    	return $this->belongsTo('App\DatosBasicos','id_parentesco');
    }
}
