<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DatosBasicos_m extends Model
{
    protected $table='datosBasicos_m';
    protected $fillable=['id','v_triple','v_polio','v_bcg','v_antirabiolica','v_rubeola','v_sarampion','v_meningitis','v_hepatitis','otros','f_sangre','padecimiento','tratamiento','medico_tratante','alergias','tratamiento2','id_datosBasicos'];

    public function datoBasico()
    {
    	return $this->belongsTo('App\DatosBasicos','id_datosBasicos','id');
    }
}
