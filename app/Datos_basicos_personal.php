<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Datos_basicos_personal extends Model
{
    protected $table='datos_basicos_personal';
    protected $fillable=['id','nombre','apellidos','nacio','cedula','rif','estado_c','genero','lugar_n','fecha_n','direccion','cod_telefono','telefono','cod_telefono_hab','telefono_hab','email','cuenta_v','cuenta_p','id_parroquia','id_ciudad','id_cargo'];

    public function parroquia()
    {
        return $this->belongsTo('App\Parroquias','id_parroquia');
    }

    public function ciudad()
    {
        return $this->belongsTo('App\Ciudades','id_ciudad');
    }

    public function cargo()
    {
    	return $this->belongsTo('App\Cargos','id_cargo');
    }

    public function inscripcion()
    {
        return $this->HasMany('App\inscripcion','id_profesor','id');
    }

}
