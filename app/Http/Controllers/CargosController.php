<?php

namespace App\Http\Controllers;

use App\Cargos;
use App\Datos_basicos_personal;
use Illuminate\Http\Request;

class CargosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $num=0;
        $cargos=Cargos::all();

        return View('admin.cargos.index', compact('num','cargos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cargo=Cargos::where('cargo',$request->cargo)->get()->count();

        if ($cargo == 0) {

            $cargo=Cargos::create([
                'cargo' => $request->cargo,
                'status' => 'si'
            ]);
            flash('Cargo registrado con éxito!')->success();
            
        }else{
            flash('ESTE CARGO YA SE ENCUENTRA REGISTRADO!')->error();
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cargos  $cargos
     * @return \Illuminate\Http\Response
     */
    public function show(Cargos $cargos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cargos  $cargos
     * @return \Illuminate\Http\Response
     */
    public function edit(Cargos $cargos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cargos  $cargos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cargos $cargos)
    {
        //
    }

    public function status(Request $request, $id)
    {
        $cargo=Cargos::find($id);

        if ($cargo->status=='si') {
            $cargo->status='no';
            $cargo->save();
        }else{
            $cargo->status='si';
            $cargo->save();
        }

        flash('Status del cargo ha sido cambiado con éxito!')->success();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cargos  $cargos
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $personal=Datos_basicos_personal::where('id_cargo', $request->id)->get()->count();

        if ($personal == 0) {
            $cargo=Cargos::find($request->id);
            $cargo->delete();

            flash('Cargo eliminado con éxito!')->success();

        }else{
            flash('ESTE CARGO YA SE ENCUENTRA ASIGNADO! Elimine el personal que está asignado para eliminar el cargo')->error();
        }

        return redirect()->back();
    }
}
