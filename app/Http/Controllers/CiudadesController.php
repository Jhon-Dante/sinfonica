<?php

namespace App\Http\Controllers;

use App\Ciudades;
use Illuminate\Http\Request;

class CiudadesController extends Controller
{
    public function buscarCiudad($id)
    {
        return $ciudad=Ciudades::where('id_estado',$id)->get();
    }
}
