<?php

namespace App\Http\Controllers;

use App\Coordinadores;
use Illuminate\Http\Request;

class CoordinadoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $coordinador=Coordinadores::where('nacio', $request->nacio)->where('cedula', $request->cedula)->get()->count();

        if($coordinador != 0){
            $create=Coordinadores::create([
                'nombre', $request->,
                'apellido', $request->,
                'nacio', $request->,
                'cedula', $request->,
                'direccion', $request->,
                'cod_telefono', $request->,
                'telefono', $request->,
                'email', $request->
            ]);

            flash('Coordinador registrado con éxito!')->success();

        }else{
            flash('Esta cédula/passaporte de este coordinador se encuentra registrado!')->warning();
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Coordinadores  $coordinadores
     * @return \Illuminate\Http\Response
     */
    public function show(Coordinadores $coordinadores)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Coordinadores  $coordinadores
     * @return \Illuminate\Http\Response
     */
    public function edit(Coordinadores $coordinadores)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Coordinadores  $coordinadores
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Coordinadores $coordinadores)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Coordinadores  $coordinadores
     * @return \Illuminate\Http\Response
     */
    public function destroy(Coordinadores $coordinadores)
    {
        //
    }
}
