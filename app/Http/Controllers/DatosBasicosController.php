<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Storage;
use Image;
use App\Datos_basicos_Personal;
use App\DatosBasicos;
use App\Representantes;
use App\DatosBasicos_e;
use App\DatosBasicos_m;
use App\DatosBasicos_has_representantes;
use App\Periodos;
use App\instrumentos;
// use App\Inscripcion;
use App\Parentesco;
use App\Programas;
use App\Inscripcion;
use App\Preinscripcion;

class DatosBasicosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $num=0;
        $periodo=Periodos::where('status','Activo')->first();
        $inscripcion=Inscripcion::where('id_periodo',$periodo->id)->get();
        $datosBasicos=datosBasicos::all();
        $datosBasicos_m=DatosBasicos_m::all();
        $datosBasicos_e=DatosBasicos_e::all();

        return View('admin.datosBasicos.index', compact('num','datosBasicos','datosBasicos_m','datosBasicos_e','inscripcion'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $datosBasicos=Inscripcion::where('id_periodo',1)->get();
        $datosBasicos=datosBasicos::all();
        $parentesco=Parentesco::all();
        $representantes=Representantes::all();
        return View('admin.datosBasicos.create', compact('representantes','datosBasicos','parentesco'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $datobasico=datosBasicos::where('nacio',$request->nacio_add)->where('cedula',$request->cedula_add)->get()->count();

        if ($datobasico>0) {
            flash('Ya existe un estudiante con esta cédula!')->error();
            return redirect()->back();
        }else{

            if ($request->vive_p_add == 'si') {;$vive_p='si';}else{;$vive_p='no';};if ($request->vive_m_add == 'si') {;$vive_m='si';}else{;$vive_m='no';};if ($request->vive_o_add == 'si') {;$vive_o='si';}else{;$vive_o='no';}


            $datosBasicos=DatosBasicos::create([
                'nombres' => $request->nombres_add,
                'apellidos' => $request->apellidos_add,
                'nacio' => $request->nacio_add,
                'cedula' => $request->cedula_add,
                'lugar_nac' => $request->lugar_nac_add,
                'fecha_nac' => $request->fecha_nac_add,
                'genero' => $request->sexo_add,
                'vive_p' => $vive_p,
                'vive_m' => $vive_m,
                'vive_o' => $vive_o,
                'vive_otros' => $request->vive_otros_add,
                'direccion' => $request->direccion_add,
                'cod_tel' => $request->cod_tel_add,
                'telefono' => $request->telefono,
                'foto' => $request->foto_add,
                'status' => 'si'

            ]);

            $estudiante=DatosBasicos::where('nacio',$request->nacio_add)->where('cedula',$request->cedula_add)->first();
            
            $path= Storage::disk('public')->put('estudiantes', $request->file('foto_add'));
            $datosBasicos->fill(['foto' => asset($path)])->save();


            $estudios=DatosBasicos_e::create([
                'institucion' => $request->nombre,
                'tipo' => $request->tipo,
                'direccion' => $request->direccion,
                'nombre_director' => $request->director,
                'grado' => $request->grado,
                'turno' => $request->turno,
                'id_datosBasicos' => $estudiante->id
            ]);

            if($request->v_triple == 'si'){$v_triple='si';}else{$v_triple='no';}if($request->v_polio == 'si'){$v_polio='si';}else{$v_polio='no';}if($request->v_bcg == 'si'){$v_bcg='si';}else{$v_bcg='no';}if($request->v_antirabiolica == 'si'){$v_antirabiolica='si';}else{$v_antirabiolica='no';}if($request->v_rubeola == 'si'){$v_rubeola='si';}else{$v_rubeola='no';}if($request->v_sarampion == 'si'){$v_sarampion='si';}else{$v_sarampion='no';}if($request->v_meningitis == 'si'){$v_meningitis='si';}else{$v_meningitis='no';}if($request->v_hepatitis == 'si'){$v_hepatitis='si';}else{$v_hepatitis='no';}


            $medicos=DatosBasicos_m::create([
                'v_triple' => $v_triple,
                'v_polio' => $v_polio,
                'v_bcg' => $v_bcg,
                'v_antirabiolica' => $v_antirabiolica,
                'v_rubeola' => $v_rubeola,
                'v_sarampion' => $v_sarampion,
                'v_meningitis' => $v_meningitis,
                'v_hepatitis' => $v_hepatitis,
                'otros' => $request->otros,
                'f_sangre' => $request->g_sanguineo,
                'padecimiento' => $request->padecimiento,
                'tratamiento' => $request->tratamiento,
                'medico_tratante' => $request->medico_tratante,
                'alergias' => $request->alergias,
                'tratamiento2' => $request->tratamiento2,
                'id_datosBasicos' => $estudiante->id
            ]);
            
            // if (count($request->id_representante) >0 ) {
            //     for ($i=0; $i < count($request->id_representante) ; $i++) { 
                    
            //         $representantes=DatosBasicos_has_representantes::create([
            //             'id_datosBasicos' => $estudiante->id,
            //             'id_representante' => $request->id_representante[$i],
            //             'id_parentesco' => $request->id_parentesco[$i],
            //             'principal' => 'no'

            //         ]);
            //     }
            // }

            flash('Estudiante registrado con éxito!')->success();

            $num=0;
            $datosBasicos=datosBasicos::all();
            $datosBasicos_m=DatosBasicos_m::all();
            $datosBasicos_e=DatosBasicos_e::all();
            $inscripcion=Inscripcion::all();

            return View('admin.datosBasicos.index', compact('num','datosBasicos','datosBasicos_m','datosBasicos_e','inscripcion'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function inscripcionCreate(Request $request)
    {
        $personal=Datos_basicos_Personal::where('status','si')->get();
        $programas=Programas::where('status','si')->get();
        $instrumentos=Instrumentos::all();
        $estudiante=DatosBasicos::find($request->id_datosBasicos);
        $periodo=Periodos::where('status','Activo')->first();

        return View('admin.datosBasicos.inscribir', compact('estudiante','periodo','instrumentos','programas','personal'));
    }
    public function show($id)
    {
        //
    }

    public function reinscripcion(Request $request)
    {

        $programas=Programas::all();
        $instrumentos=Instrumentos::all();
        $estudiante=DatosBasicos::find($request->id_datosBasicos);
        $periodo=Periodos::where('status','Activo')->first();

        

        $verifica=Inscripcion::where('id_datosBasicos', $request->id_datosBasicos)->where('id_subPrograma',$request->id_subPrograma)->where('id_periodo', $request->id_periodo)->get()->count();

        if ($verifica > 0) {
            flash('El estudiante ya se encuentra registrado en ese mismo subprograma!')->warning();
            return View('admin.datosBasicos.inscribir', compact('estudiante','periodo','instrumentos','programas'));
        }else{
                $inscribir=Inscripcion::create([
                    'id_datosBasicos' => $request->id_datosBasicos,
                    'id_subPrograma' => $request->id_subPrograma,
                    'id_periodo' => $request->id_periodo,
                    'id_instrumento' => $request->id_instrumento,
                    'fecha_audicion' => $request->audi_fecha,
                    'id_profesor' => $request->id_profesor
                ]);

                if ($inscribir) {
                    flash('Inscripción realizada con éxito!')->success();
                }else{
                    flash('Hubo un error al registrar al estudiante!')->error();
                }

                $num=0;
                $datosBasicos=datosBasicos::all();
                $datosBasicos_m=DatosBasicos_m::all();
                $datosBasicos_e=DatosBasicos_e::all();
                $inscripcion=Inscripcion::all();

                return View('admin.datosBasicos.index', compact('inscripcion','num','datosBasicos','datosBasicos_m','datosBasicos_e'));
            
        }// Fin del verificar estudiante ya registrado

    }

    public function constanciaI($id)
    {
        $inscripcion=Inscripcion::find($id);
        $año=date('Y');
        $periodo=Periodos::where('status','Activo')->first();
        
        $dompdf = \PDF::loadView('admin.pdfs.constancias.estudios', ['inscripcion' => $inscripcion, 'periodo' => $periodo, 'año' => $año]);

                    return $dompdf->stream();
    }

    public function constanciaAudicion($id)
    {
        $inscripcion=Inscripcion::find($id);
        $año=date('Y');
        $periodo=Periodos::where('status','Activo')->first();
        
        $dompdf = \PDF::loadView('admin.pdfs.constancias.audicion', ['inscripcion' => $inscripcion, 'periodo' => $periodo, 'año' => $año]);

        return $dompdf->stream();
    }

    public function constanciaPermiso(Request $request)
    {
        // dd($request->all());
        $inscripcion=Inscripcion::find($request->id);
        $dia=$request->dia;
        $hora=$request->hora;
        $motivo=$request->motivo;
        $año=date('Y');
        $periodo=Periodos::where('status','Activo')->first();
        
        $dompdf = \PDF::loadView('admin.pdfs.constancias.permiso', ['inscripcion' => $inscripcion, 'periodo' => $periodo, 'año' => $año, 'dia' => $dia, 'hora' => $hora, 'motivo' => $motivo]);

        return $dompdf->stream();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
