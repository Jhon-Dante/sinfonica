<?php

namespace App\Http\Controllers;

use App\Datos_basicos_personal;
use App\Estados;
use App\User;
use App\Cargos;
use Illuminate\Http\Request;
use App\Auditoria;

class DatosBasicosPersonalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $accion ='Visualización de listado del personal registrado';
        $this->auditoria($accion);
        $num=0;
        $cargos=Cargos::all();
        $estados=Estados::all();
        $personal=Datos_basicos_personal::all();

        return View('admin.personal.index', compact('num','cargos','estados','personal'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $buscar=Datos_basicos_personal::where('nacio',$request->nacio)->where('cedula',$request->cedula)->get()->count();
        $buscar2=Datos_basicos_personal::where('email',$request->email)->get()->count();

        if ($buscar==0) {

            if ($buscar2 ==0) {
                
                $personal=Datos_basicos_personal::create([
                    'nombre' => $request->nombre,
                    'apellidos' => $request->apellidos,
                    'nacio' => $request->nacio,
                    'cedula' => $request->cedula,
                    'rif' => $request->rif,
                    'estado_c' => $request->estado_c,
                    'genero' => $request->genero,
                    'lugar_n' => $request->lugar_n,
                    'fecha_n' => $request->fecha_n,
                    'direccion' => $request->direccion,
                    'cod_telefono' => $request->cod_telefono,
                    'telefono' => $request->telefono,
                    'cod_telefono_hab' => $request->cod_telefono_hab,
                    'telefono_hab' => $request->telefono_hab,
                    'email' => $request->email,
                    'cuenta_v' => $request->cuenta_v,
                    'cuenta_p' => $request->cuenta_p,
                    'status' => 'si',
                    'id_parroquia' => $request->id_parroquia,
                    'id_ciudad' => $request->id_ciudad,
                    'id_cargo' => $request->id_cargo,

                ]);
                
                $accion ='Registra al nuevo personal llamado '.$request->nombre.' '.$request->apellidos;
                $this->auditoria($accion);
                
                if ($request->createUser == 'si') {
                    $contraseña=rand(100000000,1000000000);

                    $usuario=User::create([
                        'name' => $request->nombre.' '.$request->apellidos,
                        'email' => $request->email,
                        'password' => bcrypt($contraseña)
                    ]);
                    flash('PERSONAL REGISTRADO CON ÉXITO! Contraseña del personal '.$contraseña)->important();
                }else{
                    flash('PERSONAL REGISTRADO CON ÉXITO!')->success();
                }


            }else{
                flash('ESTE EMAIL YA SE ENCUENTRA REGISTRADO EN EL SISTEMA!')->error();
                return redirect()->back()->WithInput();
            }
            
        }else{
            flash('ESTA CÉDULA/PASSPORT YA SE ENCUENTRA REGISTRADO EN EL SISTEMA!')->error();
            return redirect()->back()->WithInput();
        }

        $num=0;
        $cargos=Cargos::all();
        $estados=Estados::all();
        $personal=Datos_basicos_personal::all();

        return View('admin.personal.index', compact('num','cargos','estados','personal'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Datos_basicos_personal  $datos_basicos_personal
     * @return \Illuminate\Http\Response
     */
    public function show(Datos_basicos_personal $datos_basicos_personal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Datos_basicos_personal  $datos_basicos_personal
     * @return \Illuminate\Http\Response
     */
    public function edit(Datos_basicos_personal $datos_basicos_personal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Datos_basicos_personal  $datos_basicos_personal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Datos_basicos_personal $datos_basicos_personal)
    {
        //
    }

    public function status(Request $request, $id)
    {
        $personal=Datos_basicos_personal::find($id);

        if ($personal->status=='si') {
            $personal->status='no';
            $personal->save();
        }else{
            $personal->status='si';
            $personal->save();
        }

        $accion ='Cambia el status del empleado '.$personal->nombre.' '.$personal->apellidos;
        $this->auditoria($accion);

        flash('Status del empleado ha sido cambiado con éxito!')->success();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Datos_basicos_personal  $datos_basicos_personal
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $personal=Datos_basicos_personal::find($request->id);
        $nombre= $personal->nombre.' '.$personal->apellido;
        $personal->delete();

        $accion ='Elimina al empleado '.$nombre;
        $this->auditoria($accion);

        flash('Personal eliminado con éxito!')->success();

        return redirect()->back();
    }

    private function auditoria($accion)
    {
        $auditoria=Auditoria::create([
                    'id_user' => \Auth::user()->id,
                    'accion' => $accion
                ]);
    }
}
