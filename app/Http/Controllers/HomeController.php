<?php


namespace App\Http\Controllers;

use App\User;
use App\Auditoria;
/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        if(auth()->user()->status == "si" ){
            return view('adminlte::home');
        }else{
            flash('¡No está autorizado para entrar al sistema!')->error();
            \Auth::logout();
            return redirect('/login');
        }
    }
}
