<?php

namespace App\Http\Controllers;

use App\Instrumentos;
use App\TipoInstrumentos;
use App\Periodos;
use App\Inscripcion;
use Illuminate\Http\Request;

class InstrumentosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $num=0;
        $tipos=TipoInstrumentos::all();
        $instrumentos=Instrumentos::all();

        return view('admin.instrumentos.index', compact('num','instrumentos','tipos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $instrumento=Instrumentos::where('serial',$request->serial)->get()->count();    
        $instrumento2=Instrumentos::where('inventario',$request->inventario)->get()->count();
        if ($instrumento != null) {
            flash('¡Ya se encuentra un instrumento registrado con este serial! Especifique otro!')->error();
            return redirect()->back()->withInput();
        }else{
            if ($instrumento2 != null) {
                flash('¡Ya se encuentra un instrumento registrado con este inventario! Especifique otro!')->error();
                return redirect()->back()->withInput();
            }else{

                if ($request->tipo_m == null || $request->ancho_m == null) {
                    flash('¡Especifique de forma correcta las medidas del instrumento!')->error();
                    return redirect()->back()->withInput();
                }else{

                    $instrumentos=Instrumentos::create([
                        'nombre' => $request->nombre,
                        'tipo' => $request->tipo,
                        'marca' => $request->marca,
                        'modelo' => $request->modelo,
                        'tipo_m' => $request->tipo_m,
                        'ancho_m' => $request->ancho_m,
                        'largo_m' => $request->largo_m,
                        'serial' => $request->serial,
                        'inventario' => $request->inventario,
                        'detalles' => $request->detalles,
                        'status' => 'si',
                        'id_clasif' => $request->id_clasif
                    ]);

                    flash('Instrumento registrado con éxito!')->success();
                }//Medidas
            }//Inventario
        }//Serial
        
        
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Instrumentos  $instrumentos
     * @return \Illuminate\Http\Response
     */
    public function show(Instrumentos $instrumentos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Instrumentos  $instrumentos
     * @return \Illuminate\Http\Response
     */
    public function edit(Instrumentos $instrumentos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Instrumentos  $instrumentos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Instrumentos $instrumentos)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Instrumentos  $instrumentos
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $id=$request->id;
        $periodo=Periodos::where('status', 'activo')->first();
        $estudiante=Inscripcion::where('id_instrumento',$id)->where('id_periodo',$periodo->id)->first();

        if ($estudiante == null) {

            $instrumento=Instrumentos::find($id);
            $instrumento->delete();

            flash('Instrumento eliminado con éxito!')->success();
        }else{
            flash('ESTE INSTRUMENTO HA SIDO ASIGNADO A UN ESTUDIANTE! ELIMINE LA ASIGNACIÓN PRIMERO!')->warning();
        }

        return redirect()->back();
    }
}
