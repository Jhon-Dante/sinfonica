<?php

namespace App\Http\Controllers;

use App\SubProgramas;
use App\Programas;
use App\Materias;
use Illuminate\Http\Request;

class MateriasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $num=0;
        $materias=Materias::all();
        $programas=Programas::where('status','si')->get();
        
        return View('admin.materias.index', compact('num','materias','programas'));
    }


    // public function subprograma($id)
    // {
    //     return $subprograma=SubProgramas::where('id_programa',$id)->get();
    // }
    public function buscarMateria($id)
    {
        return $materia=Materias::where('id_subPrograma',$id)->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $materias=Materias::create([
            'materia' => $request->materia,
            'status' => 'si',
            'id_subPrograma' => $request->id_subPrograma
        ]);

        flash('Materia registrada con éxito!')->success();        
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Materias  $materias
     * @return \Illuminate\Http\Response
     */
    public function show(Materias $materias)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Materias  $materias
     * @return \Illuminate\Http\Response
     */
    public function edit(Materias $materias)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Materias  $materias
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Materias $materias)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Materias  $materias
     * @return \Illuminate\Http\Response
     */
    public function destroy(Materias $materias)
    {
        //
    }
}
