<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Estados;
use App\Municipios;
use App\Modulos;
use App\Http\Requests\ModulosRequest;

class ModulosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $estados=Estados::all();
        $num=0;
        $modulos=Modulos::paginate();


        return View('admin.modulos.index', compact('num','modulos','estados'));
    }    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $file = $request->file('foto_add');
        $nombre= $file->getClientOriginalName();


        // dd($nombre);
        $Modulos=Modulos::where('rif',$request->rif_add)->count();

        if ($Modulos>0) {
            flash('Este rif ya se encuentra registrado!')->error();
            return redirect()->back();
        }else{
            $registro=Modulos::create([
                'nombre' => $request->nombre_add,
                'rif' =>    $request->rif_add,
                'direccion' => $request->direccion_add,
                'tipo' => $request->tipo,
                'ocupacion' => $request->ocupacion_add,
                'cod_telefono' => $request->codigo_telefono_add,
                'telefono' => $request->telefono_add,
                'email' => $request->email_add,
                'detalles' => $request->detalles_add,
                'nucleo_p' => 'no',
                'status'  => 'si',
                'foto' => '/local/'.$nombre,
                'id_parroquia' => $request->id_parroquia,
                'id_ciudad' => $request->id_ciudad
            ]);

            \Storage::disk('local')->put($nombre, \File::get($file));
            flash('Registro del nuevo módulo ha sido exitoso!')->success();
            return redirect()->back();
        }//SI RIF EXISTE
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Modulos  $Modulos
     * @return \Illuminate\Http\Response
     */
    public function show(Modulos $Modulos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Modulos  $Modulos
     * @return \Illuminate\Http\Response
     */
    public function edit(Modulos $Modulos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Modulos  $Modulos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Modulos $Modulos)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Modulos  $Modulos
     * @return \Illuminate\Http\Response
     */
    public function destroy(Modulos $Modulos)
    {
        //
    }
}
