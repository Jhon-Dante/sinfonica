<?php

namespace App\Http\Controllers;

use App\Municipios;
use Illuminate\Http\Request;

class MunicipiosController extends Controller
{
    public function buscarMunicipio($id)
    {
        return $municipio=Municipios::where('id_estado',$id)->get();
    }
}
