<?php

namespace App\Http\Controllers;

use App\Parroquias;
use Illuminate\Http\Request;

class ParroquiasController extends Controller
{
    public function buscarParroquia($id)
    {
        return $parroquia=Parroquias::where('id_municipio',$id)->get();
    }
}
