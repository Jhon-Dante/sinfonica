<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use App\Http\Requests\PeriodosRequest;
use App\Periodos;

class PeriodosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $num=0;
        $periodos=Periodos::all();

        return View('admin.periodos.index', compact('periodos','num'));
    }

   public function storePeriodo(PeriodosRequest $request)
   {

        $periodo=Periodos::where('periodo', $request->periodo)->get();

        if (count($periodo)!=0) {
            $mensaje='Este periodo ya se encuentra registrado!';
        }else{

            $registro=Periodos::create([
                'periodo' => $request->periodo,
                'status' => 1
            ]);

            flash('periodo registrado con exito!');

        }

   }


   public function listarPeriodo(Request $request)
   {
       return $periodo=Periodos::all();
   }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Periodos  $periodos
     * @return \Illuminate\Http\Response
     */
    public function show(Periodos $periodos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Periodos  $periodos
     * @return \Illuminate\Http\Response
     */
    public function edit(Periodos $periodos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Periodos  $periodos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Periodos $periodos)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Periodos  $periodos
     * @return \Illuminate\Http\Response
     */
    public function destroy(Periodos $periodos)
    {
        //
    }
}
