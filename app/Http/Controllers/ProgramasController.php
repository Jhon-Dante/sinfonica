<?php

namespace App\Http\Controllers;

use App\Modulos;
use App\Programas;
use App\Modulos_has_programas;
use Illuminate\Http\Request;

class ProgramasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $programas=Programas::groupBy('programa')->get();
        $programaM=Modulos_has_programas::all();
        $num=0;
        $modulos=Modulos::all();

        return View('admin.programas.index', compact('num','programas','modulos','programaM'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $programas=Programas::create([
            'programa' => $request->programa,
            'status' => 'si',
        ]);

        for ($i=0; $i < count($request->id_modulo) ; $i++) { 
            
            $programa=Modulos_has_programas::where('id_modulo',$request->id_modulo[$i])->where('id_programa',$programas->id)->count();

            if ($programa==null) {
                $programaModulo=Modulos_has_programas::create([
                    'id_modulo' => $request->id_modulo[$i],
                    'id_programa' => $programas->id
                ]);
                flash('Programa registrado con éxito!')->success();        
            }else{
                flash('Este programa ya se encuentra registrado o ya está asignado en el módulo <strong>'.$modulo->nombre.'</strong>, especifique otro!')->error();
            }
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Programas  $programas
     * @return \Illuminate\Http\Response
     */
    public function show(Programas $programas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Programas  $programas
     * @return \Illuminate\Http\Response
     */
    public function edit(Programas $programas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Programas  $programas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Programas $programas)
    {
        //
    }

    public function statusPrograma($id)
    {
        // dd($id);
        $programa=Programas::find($id);

        if ($programa->status=='si') {
            $programa->status='no';
            $programa->save();
        }else{
            $programa->status='si';
            $programa->save();
        }

        flash('Status del programa cambiado con éxito!');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Programas  $programas
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        // dd($request->id);
        $programa=Programas::find($request->id);

        if ($programa->delete()) {
            flash('Programa eliminado con éxito!')->success();        
            return redirect()->back();
        }else{
            flash('Ocurrió un error al eliminar el programa!')->danger();        
            return redirect()->back();
        }
    }
}
