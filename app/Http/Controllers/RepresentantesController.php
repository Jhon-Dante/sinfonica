<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use App\Http\Request\RepresentantesRequest;
use Illuminate\Support\Facades\Storage;
use App\Representantes;
use Image;

class RepresentantesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $num=0;
        $representantes=Representantes::paginate();

        return View('admin.representantes.index', compact('num','representantes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $email=Representantes::where('email', $request->email_add)->get()->count();
        $cedula=Representantes::where('cedula', $request->cedula_add)->get()->count();

        if ($email>0) {
            flash('Este email ya se encuentra registrado!')->error();
        }else{

            if ($cedula>0) {
                flash('Esta cédula ya se encuentra registrada!')->error();
            }else{

                if ($request->file('foto_add') == false) {
                    flash('Seleccione una foto del representante!');
                }else{

                    $representante=Representantes::create([
                        'nombres'  =>       $request->nombres_add,
                        'apellidos' =>      $request->apellidos_add,
                        'nacio' =>          $request->nacio_add,
                        'cedula' =>         $request->cedula_add,
                        'genero' =>         $request->genero_add,
                        'profesion' =>      $request->profesion_add,
                        'direccion' =>      $request->direccion_add,
                        'email' =>          $request->email_add,
                        'cod_hab' =>        $request->cod_hab_add,
                        'telefono_hab' =>   $request->telefono_hab_add,
                        'cod_cel' =>        $request->cod_cel_add,
                        'telefono_cel' =>   $request->telefono_cel_add,
                        'lugar_tra' =>      $request->lugar_tra_add,
                        'cargo_tra' =>      $request->cargo_tra_add,
                        'cod_opc' =>        $request->cod_opc_add,
                        'telefono_opc' =>   $request->telefono_opc_add,
                        'foto' =>           $request->foto_add,
                        'id_modulo' =>      1
                    ]);

                   $path= Storage::disk('public')->put('representantes', $request->file('foto_add'));
                   $representante->fill(['foto' => asset($path)])->save();

                    if ($representante) {
                        flash('Representante registrado con éxito!')->success();
                    }else{
                        flash('Hubo un error en el registro!')->error();
                    }
                }//Fin de la condicional foto
            }//Fin de la condicional de cedula
        }//Fin de la condicional de email
        

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Representantes  $representantes
     * @return \Illuminate\Http\Response
     */
    public function show(Representantes $representantes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Representantes  $representantes
     * @return \Illuminate\Http\Response
     */
    public function edit(Representantes $representantes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Representantes  $representantes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Representantes $representantes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Representantes  $representantes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Representantes $representantes)
    {
        //
    }
}
