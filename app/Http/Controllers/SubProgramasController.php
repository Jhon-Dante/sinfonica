<?php

namespace App\Http\Controllers;

use App\Programas;
use App\SubProgramas;
use Illuminate\Http\Request;

class SubProgramasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $num=0;
        $subprogramas=SubProgramas::all();
        $programas=Programas::where('status','si')->get();

        return View('admin.sub_programas.index', compact('num','subprogramas','programas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sub_programa=SubProgramas::create([
            'sub_programa' => $request->sub_programa,
            'status' => 'si',
            'id_programa' => $request->id_programa
            
        ]);

        flash('Sub-programa registrado con éxito!')->success();        
        return redirect()->back();
    }

    public function subprograma($id)
    {
        return $subprograma=SubProgramas::where('id_programa',$id)->get();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SubProgramas  $subProgramas
     * @return \Illuminate\Http\Response
     */
    public function show(SubProgramas $subProgramas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SubProgramas  $subProgramas
     * @return \Illuminate\Http\Response
     */
    public function edit(SubProgramas $subProgramas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SubProgramas  $subProgramas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SubProgramas $subProgramas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SubProgramas  $subProgramas
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubProgramas $subProgramas)
    {
        //
    }
}
