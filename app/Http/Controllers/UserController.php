<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Auditoria;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $num=0;
        $usuarios=User::all();
        return View('admin.usuarios.index', compact('num','usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function status(Request $request, $id)
    {
        $usuario=User::find($id);

        if ($usuario->status=='si') {
            $usuario->status='no';
            $usuario->save();
        }else{
            $usuario->status='si';
            $usuario->save();
        }

        $accion ='Cambia el status del usuario '.$usuario->name;
        $this->auditoria($accion);

        flash('Status del usuario ha sido cambiado con éxito!')->success();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function permisos(Request $request)
    {
        if (\Auth::user()->tipo_user == 'Admin' || 
            \Auth::user()->tipo_user == 'Director' || $request->contraseña == null) {
            $usuario=User::where('email',\Auth::user()->email)->first();
            $clave=$request->password;
            $validator=\Auth::user()->password;

            if (password_verify($clave, $validator)) {
                $num=0;
                $usuarios=User::all();

                $accion ='Entra ingresa en los permisos de usuarios';
                $this->auditoria($accion);

                return View('admin.usuarios.edit_per', compact('num','usuarios'));

                
            }else{
                flash('¡CONTRASEÑA INCORRECTA!')->error();
                return redirect()->back();
            }
        }else{
            flash('NO ESTÁ AUTORIZADO PARA ACCEDER A ESTA FUNCIONALIDAD!')->error();
            return redirect()->back();
        }
    }

    public function editPer(Request $request)
    {
        // dd($request->all());
        for ($i=0; $i < count($request->id) ; $i++) { 
            $user=User::find($request->id[$i]);
            // dd($request->pre_re[$i]);
            if($request->pre_re[$i] == null){$user->pre_re = 'No';}else{$user->pre_re = 'Si';}
            if($request->list_estu[$i] == null){$user->list_estu = 'No';}else{$user->list_estu = 'Si';}
            if($request->edit_estu[$i] == null){$user->edit_estu = 'No';}else{$user->edit_estu = 'Si';}
            if($request->eli_estu[$i] == null){$user->eli_estu = 'No';}else{$user->eli_estu = 'Si';}
            if($request->const_estu[$i] == null){$user->const_estu = 'No';}else{$user->const_estu = 'Si';}
            if($request->cer_estu[$i] == null){$user->cer_estu = 'No';}else{$user->cer_estu = 'Si';}
            if($request->titulob_estu[$i] == null){$user->titulob_estu = 'No';}else{$user->titulob_estu = 'Si';}
            $user->save();
        }

        $accion ='Cambia los permisos de usuarios';
        $this->auditoria($accion);

        flash('Permisos editados con éxito!')->success();
        $num=0;
        $usuarios=User::all();
        return View('admin.usuarios.edit_per', compact('num','usuarios'));
    }

    private function auditoria($accion)
    {
        $auditoria=Auditoria::create([
                    'id_user' => \Auth::user()->id,
                    'accion' => $accion
                ]);
    }
}
