<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NucleosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre_add' => 'required',
            // 'rif_add'   =>  'required|numeric|regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-]*)*)+$/',
            // 'id_estado' =>  'required|regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-]*)*)+$/',
            // 'id_municipio'  =>  'required|regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-]*)*)+$/',
            'id_ciudad' =>  'required',
            'id_parroquia'  =>  'required'
            // 'tipo_add'  =>  'required|regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-]*)*)+$/',
            // 'ocupacion_add' =>  'required|regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-]*)*)+$/',
            // 'direccion_add' =>  'required|regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-]*)*)+$/',
            // 'codigo_telefono_add'   =>  'required|numeric|regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-]*)*)+$/',
            // 'telefono_add'  =>  'required|numeric|regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-]*)*)+$/',
            // 'email_add' =>  'required|regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-]*)*)+$/' 
        ];
    }

        // public function messages()
        // {
        //     return [
        //         'nombre.required'      =>           'Escriba el nombre de la institución',
        //         'rif_add.required' =>               'Escriba el rif válido de la institución',
        //         'id_estado.required' =>             'Seleccione el estado de la institución',
        //         'id_municipio.required' =>          'Seleccione el municipio de la institución',
        //         'id_ciudad.required' =>             'Seleccione la ciudad de la institución',
        //         'id_parroquia.required' =>          'Seleccine la parroquia de la institución',
        //         'tipo_add.required' =>              'Seleccione si la institución es pública o privada',
        //         'ocupacion_add.required' =>         'Especifique a que se dedica la institución',
        //         'direccion_add.required' =>         'Especifique la dirección exacta de la institución',
        //         'codigo_telefono_add.required' =>   'Especifique el código de telefono de la institución',
        //         'telefono_add.required' =>          'transcriba el teléfono de la institución sin el código',
        //         'email_add.required' =>             'Especifique el email de la institución'
        //     ];
        // }
}
