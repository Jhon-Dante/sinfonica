<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inscripcion extends Model
{
    protected $table='inscripcion';
    protected $fillable=['id','id_datosBasicos','id_subPrograma','id_periodo','id_instrumento','fecha_audicion','id_profesor'];

    public function datoBasico()
    {
    	return $this->belongsTo('App\DatosBasicos','id_datosBasicos','id');
    }
   	
   	public function sub_programa()
    {
    	return $this->belongsTo('App\SubProgramas','id_subPrograma','id');
    }

    public function periodo()
    {
    	
    	return $this->belongsTo('App\Periodos','id_periodo','id');
    }

    public function instrumento()
    {
    	return $this->belongsTo('App\Instrumentos','id_instrumento','id');
    }

    public function personal()
    {
        return $this->belongsTo('App\Datos_basicos_personal','id_profesor','id');
    }
}
