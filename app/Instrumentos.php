<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Instrumentos extends Model
{
    protected $table='instrumentos';
    protected $fillable=['id','nombre','tipo','marca','modelo','ancho_m','largo_m','tipo_m','serial','inventario','detalles','status','id_clasif'];

    public function clasif()
    {
    	return $this->belongsTo('App\TipoInstrumentos','id_clasif');
    }

    public function inscripcion()
    {
    	return $this->HasMany('App\Inscripcion ','id_instrumento','id');
    }
}
