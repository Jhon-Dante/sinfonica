<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Materias extends Model
{
    protected $table='materias';
    protected $fillable=['id','materia','status','id_subPrograma'];

    public function sub_programa()
    {
    	return $this->belongsTo('App\SubProgramas','id_subPrograma');
    }
}
