<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modulos extends Model
{
    protected $table='modulos';
   	protected $fillable=['id','nombre','rif','direccion','tipo','ocupacion','cod_telefono','telefono','email','detalles','nucleo_p','status','foto','id_parroquia','id_ciudad'];

   	public function parroquia()
   	{
   		return $this->belongsTo('App\Parroquias','id_parroquia');
   	}

   	public function ciudad()
   	{
   		return $this->belongsTo('App\Ciudades','id_ciudad');
   	}

      public function representante()
      {
         return $this->HasMany('App\Representantes','id_modulo','id');
      }

      public function modulos_has_programas()
      {
         return $this->HasMany('App\Modulos_has_programas','id_modulo','id');
      }


}
