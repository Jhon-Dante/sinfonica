<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modulos_has_programas extends Model
{
   protected $table='modulos_has_programas';
   protected $fillable=['id','id_modulo','id_programa'];

   public function modulo()
   {
   		return $this->belongsTo('App\Modulos','id_modulo');
   }

   public function programa()
   {
   		return $this->belongsTo('App\Programas','id_programa');
   }
}
