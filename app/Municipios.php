<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Municipios extends Model
{
    protected $table='municipios';
    protected $filable=['id','municipio','id_estado'];


    public function estado()
    {
    	return $this->belongsTo('App\Estados','id_estado');
    }

    public function parroquia()
    {
    	return $this->belongsTo('App\Parroquias','id_municipio');
    }
}
