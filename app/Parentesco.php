<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parentesco extends Model
{
    protected $table='parentesco';
    protected $filable=['id','parentesco'];

    public function datoBasico_has_representante()
    {
    	return $this->HasMany('App\DatosBasicos_has_representantes ','id_parentesco','id');
    }
}
