<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parroquias extends Model
{
    protected $table='parroquias';
    protected $filable=['id','parroquia','id_municipio'];


    public function municipio()
    {
    	return $this->belongsTo('App\Municipios','id_municipio');
    }


    public function nucleo()
    {
    	return $this->HasMany('App\Modulos','id_parroquia','id');
    }

    public function personal()
    {
        return $this->HasMany('App\Datos_basicos_personal','id_parroquia','id');
    }
}
