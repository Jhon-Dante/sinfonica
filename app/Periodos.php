<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Periodos extends Model
{
    protected $table='periodos';
    protected $fillable=['id','periodo','status'];

    public function inscripcion()
    {
    	return $this->HasMany('App\Inscripcion ','id_periodo','id');
    }
}
