<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Programas extends Model
{
    protected $table='programas';
    protected $fillable=['id','programa','status',];

    public function sub_programa()
    {
    	return $this->HasMany('App\SubProgramas','id_programa','id');
    }

    public function modulos_has_programas()
      {
         return $this->HasMany('App\Modulos_has_programas','id_programa','id');
      }
}
