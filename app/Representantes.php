<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Representantes extends Model
{
    protected $table='representantes';
    protected $fillable=['nombres','apellidos','nacio','cedula','genero','profesion','direccion','email','cod_hab','telefono_hab','cod_cel','telefono_cel','lugar_tra','cargo_tra','cod_opc','telefono_opc','foto','id_modulo'];

    public function modulo()
    {
    	return $this->belongsTo('App\Modulos','id_modulo');
    }

    public function datoBasico_has_representante()
    {
    	return $this->HasMany('App\DatosBasicos_has_representantes ','id_representantes','id');
    }
}
