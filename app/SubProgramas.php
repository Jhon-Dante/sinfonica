<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubProgramas extends Model
{
    protected $table='sub_programas';
    protected $fillable=['id','sub_programa','status','id_programa'];

    public function programa()
    {
    	return $this->belongsTo('App\Programas','id_programa');
    }

    public function materia()
    {
    	return $this->HasMany('App\Materias','id_subPrograma','id');
    }

    public function inscripcion()
    {
        return $this->HasMany('App\Inscripcion ','id_subPrograma','id');
    }

}
