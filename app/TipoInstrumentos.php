<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoInstrumentos extends Model
{
    protected $table='tipo_instrumentos';
    protected $filable=['id','tipo'];


    public function instrumento()
    {
    	return $this->hasMany('App\Instrumentos','id_tipo','id');
    }
}
