<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');

            $table->enum('tipo_user',['Admin','Director','Coordinador(a)','Supervisor(a)','Administrador(a)','Contador(a)','Utilero(a)','Mantenimiento']);
            $table->string('foto');

            //---------------------------------- Estudiante
            $table->enum('pre_re',['Si','No']);
            $table->enum('list_estu',['Si','No']);
            $table->enum('edit_estu',['Si','No']);
            $table->enum('eli_estu',['Si','No']);
            $table->enum('const_estu',['Si','No']);
            $table->enum('cer_estu',['Si','No']);
            $table->enum('titulob_estu',['Si','No']);

            $table->enum('status',['si','no']);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
