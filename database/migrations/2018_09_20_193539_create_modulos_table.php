<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModulosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modulos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('rif');
            $table->text('direccion');
            $table->enum('tipo',['publica','privada']);
            $table->string('ocupacion');
            $table->integer('cod_telefono');
            $table->integer('telefono');
            $table->string('email');
            $table->string('detalles')->nullable();
            $table->enum('nucleo_p',['si','no']);
            $table->enum('status',['si','no']);
            $table->string('foto')->nullable();
            $table->integer('id_parroquia')->unsigned();
            $table->integer('id_ciudad')->unsigned();
            $table->integer('id_coordinador')->unsigned()->nullable();

            $table->foreign('id_parroquia')->references('id')->on('parroquias')->onDelete('cascade');
            $table->foreign('id_ciudad')->references('id')->on('ciudades')->onDelete('cascade');
            $table->foreign('id_coordinador')->references('id')->on('coordinadores')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modulos');
    }
}
