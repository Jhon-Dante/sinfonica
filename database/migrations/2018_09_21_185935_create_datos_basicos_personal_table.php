<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatosBasicosPersonalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datos_basicos_personal', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('apellidos');
            $table->string('nacio');
            $table->string('cedula');
            $table->string('rif');
            $table->string('estado_c')->nullable();
            $table->string('genero')->nullable();
            $table->string('lugar_n')->nullable();
            $table->string('fecha_n')->nullable();
            $table->string('direccion')->nullable();
            $table->integer('cod_telefono')->nullable();
            $table->integer('telefono')->nullable();
            $table->integer('cod_telefono_hab')->nullable();
            $table->integer('telefono_hab')->nullable();
            $table->string('email')->nullable();
            $table->string('cuenta_v')->nullable();
            $table->string('cuenta_p')->nullable();
            $table->enum('status',['si','no']);

            $table->integer('id_parroquia')->unsigned()->nullable();
            $table->integer('id_ciudad')->unsigned()->nullable();
            $table->integer('id_cargo')->unsigned();

            $table->foreign('id_parroquia')->references('id')->on('parroquias')->onDelete('cascade');
            $table->foreign('id_ciudad')->references('id')->on('ciudades')->onDelete('cascade');
            $table->foreign('id_cargo')->references('id')->on('cargos')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datos_basicos_personal');
    }
}
