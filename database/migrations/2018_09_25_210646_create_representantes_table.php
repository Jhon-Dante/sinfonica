<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRepresentantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('representantes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombres');
            $table->string('apellidos');
            $table->enum('nacio',['V','E']);
            $table->integer('cedula');
            $table->enum('genero',['m','f','otros']);
            $table->string('profesion');
            $table->text('direccion');
            $table->string('email');
            $table->integer('cod_hab');
            $table->integer('telefono_hab');
            $table->integer('cod_cel');
            $table->integer('telefono_cel');
            $table->string('lugar_tra');
            $table->string('cargo_tra');
            $table->integer('cod_opc')->nullable();
            $table->integer('telefono_opc')->nullable();
            $table->string('foto')->nullable();
            $table->integer('id_modulo')->unsigned();

            $table->foreign('id_modulo')->references('id')->on('modulos')->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('representantes');
    }
}
