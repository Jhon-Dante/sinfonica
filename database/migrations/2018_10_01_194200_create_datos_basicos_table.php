<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatosBasicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datosBasicos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombres');
            $table->string('apellidos');
            $table->enum('nacio',['V','E']);
            $table->integer('cedula');
            $table->string('lugar_nac');
            $table->date('fecha_nac');
            $table->enum('genero',['m','f','otros']);
            $table->enum('vive_p',['si','no']);
            $table->enum('vive_m',['si','no']);
            $table->enum('vive_o',['si','no']);
            $table->string('vive_otros')->nullable();
            $table->text('direccion');
            $table->integer('cod_tel');
            $table->integer('telefono');
            $table->string('foto')->nullable();
            $table->enum('status',['si','no']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datosBasicos');
    }
}
