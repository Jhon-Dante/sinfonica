<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatosBasicosMTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datosBasicos_m', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('v_triple',['si','no']);
            $table->enum('v_polio',['si','no']);
            $table->enum('v_bcg',['si','no']);
            $table->enum('v_antirabiolica',['si','no']);
            $table->enum('v_rubeola',['si','no']);
            $table->enum('v_sarampion',['si','no']);
            $table->enum('v_meningitis',['si','no']);
            $table->enum('v_hepatitis',['si','no']);
            $table->string('otros')->nullable();
            $table->string('f_sangre');
            $table->string('padecimiento')->nullable();
            $table->string('tratamiento')->nullable();
            $table->string('medico_tratante')->nullable();
            $table->string('alergias')->nullable();
            $table->string('tratamiento2')->nullable();
            $table->integer('id_datosBasicos')->unsigned();

            $table->foreign('id_datosBasicos')->references('id')->on('datosBasicos')->onDelete('cascade');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datosBasicos_m');
    }
}
