<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatosBasicosETable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datosBasicos_e', function (Blueprint $table) {
            $table->increments('id');
            $table->string('institucion');
            $table->enum('tipo',['publica','privada']);
            $table->text('direccion');
            $table->string('nombre_director')->nullable();
            $table->string('grado');
            $table->enum('turno',['mañana','tarde','noche']);
            $table->integer('id_datosBasicos')->unsigned();

            $table->foreign('id_datosBasicos')->references('id')->on('datosBasicos')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datosBasicos_e');
    }
}
