<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatosBasicosHasRepresentantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datosBasicos_has_representantes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_datosBasicos')->unsigned();
            $table->integer('id_representantes')->unsigned();
            $table->integer('id_parentesco')->unsigned();
            $table->enum('principal',['si','no']);

            $table->foreign('id_datosBasicos')->references('id')->on('datosBasicos')->onDelete('cascade');
            $table->foreign('id_representantes')->references('id')->on('representantes')->onDelete('cascade');
            $table->foreign('id_parentesco')->references('id')->on('parentesco')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datosBasicos_has_representantes');
    }
}
