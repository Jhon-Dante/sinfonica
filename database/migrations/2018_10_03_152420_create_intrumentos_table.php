<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIntrumentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instrumentos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('tipo');
            $table->string('marca');
            $table->string('modelo');
            $table->integer('ancho_m')->nullable();
            $table->integer('largo_m')->nullable();
            $table->integer('tipo_m')->nullable();
            $table->bigInteger('serial');
            $table->bigInteger('inventario');
            $table->text('detalles')->nullable();
            $table->enum('status',['si','no']);
            $table->integer('id_clasif')->unsigned();

            $table->foreign('id_clasif')->references('id')->on('tipo_instrumentos')->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('intrumentos');
    }
}
