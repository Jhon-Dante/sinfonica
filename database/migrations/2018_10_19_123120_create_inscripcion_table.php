<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInscripcionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inscripcion', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_datosBasicos')->unsigned();
            $table->integer('id_subPrograma')->unsigned();
            $table->integer('id_periodo')->unsigned();
            $table->integer('id_instrumento')->unsigned()->nullable();
            $table->date('fecha_audicion');
            $table->integer('id_profesor')->unsigned();

            $table->foreign('id_datosBasicos')->references('id')->on('datosBasicos')->onDelete('cascade');
            $table->foreign('id_subPrograma')->references('id')->on('sub_programas')->onDelete('cascade');
            $table->foreign('id_periodo')->references('id')->on('periodos')->onDelete('cascade');
            $table->foreign('id_instrumento')->references('id')->on('instrumentos')->onDelete('cascade');
            $table->foreign('id_profesor')->references('id')->on('datos_basicos_personal')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inscripcion');
    }
}
