<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAudicionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audicion', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha_audicion');
            $table->string('calificacion');
            $table->integer('id_personal')->unsigned()->nullable();
            $table->integer('id_inscripcion')->unsigned();

            $table->foreign('id_personal')->references('id')->on('datos_basicos_personal')->onDelete('cascade');
            $table->foreign('id_inscripcion')->references('id')->on('inscripcion')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('audicion');
    }
}
