<?php

use Illuminate\Database\Seeder;

class CargosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cargos')->insert([
			'cargo' => 'Director',
			'status' => 'si'
		]);

		DB::table('cargos')->insert([
			'cargo' => 'Coordinador(a)',
			'status' => 'si'
		]);

		DB::table('cargos')->insert([
			'cargo' => 'Supervisor(a)',
			'status' => 'si'
		]);

		DB::table('cargos')->insert([
			'cargo' => 'Administrador(a)',
			'status' => 'si'
		]);
		DB::table('cargos')->insert([
			'cargo' => 'Contador(a)',
			'status' => 'si'
		]);
		DB::table('cargos')->insert([
			'cargo' => 'Operador(a) Técnico',
			'status' => 'si'
		]);

		DB::table('cargos')->insert([
			'cargo' => 'Utilero(a)',
			'status' => 'si'
		]);

		DB::table('cargos')->insert([
			'cargo' => 'Mantenimiento',
			'status' => 'si'
		]);

    }
}
