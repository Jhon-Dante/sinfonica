<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(EstadosTableSeeder::class);
        $this->call(MunicipiosTableSeeder::class);
        $this->call(CiudadesTableSeeder::class);
        $this->call(ParroquiasTableSeeder::class);
        $this->call(ParentescoTableSeeder::class);
        $this->call(ModuloTableSeeder::class);
        $this->call(PeriodosTableSeeder::class);
        $this->call(TipoInstrumentosTableSeeder::class);
        $this->call(CargosTableSeeder::class);
    }
}
