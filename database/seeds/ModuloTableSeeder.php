<?php

use Illuminate\Database\Seeder;

class ModuloTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modulos')->insert([
			'nombre' => 'Orquesta Sinfónica "Simón Bolivar, La Victoria"',
			'rif' => '45343245342',
			'direccion' => 'dsfasdfasdf',
			'tipo' => 'publica',
			'ocupacion' => 'Liceo',
			'cod_telefono' => '426',
			'telefono' => '3245342',
			'email' => 'dfg@live.com',
			'detalles' => 'asdfasfasfadsf',
			'nucleo_p' => 'si',
			'status' => 'si',
			'foto' => 'logo1.jpg',
			'id_parroquia' => '122',
			'id_ciudad' =>'49'
		]);
    }
}
