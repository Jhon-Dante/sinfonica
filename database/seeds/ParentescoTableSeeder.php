<?php

use Illuminate\Database\Seeder;

class ParentescoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('parentesco')->insert([
			'parentesco' => 'Madre'
		]);

		DB::table('parentesco')->insert([
			'parentesco' => 'Padre'
		]);

		DB::table('parentesco')->insert([
			'parentesco' => 'Hermano(a)'
		]);

		DB::table('parentesco')->insert([
			'parentesco' => 'Tio(a)'
		]);

		DB::table('parentesco')->insert([
			'parentesco' => 'Abuelo(a)'
		]);

		DB::table('parentesco')->insert([
			'parentesco' => 'Primo(a)'
		]);

		DB::table('parentesco')->insert([
			'parentesco' => 'Sobrino(a)'
		]);

		DB::table('parentesco')->insert([
			'parentesco' => 'Ahijado(a)'
		]);

		DB::table('parentesco')->insert([
			'parentesco' => 'Padrino(a)'
		]);
    }
}
