<?php

use Illuminate\Database\Seeder;

class TipoInstrumentosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('tipo_instrumentos')->insert(array(
        		'tipo' => 'Cordófonos'
        ));
        \DB::table('tipo_instrumentos')->insert(array(
        		'tipo' => 'Aerófonos'
        ));
        \DB::table('tipo_instrumentos')->insert(array(
        		'tipo' => 'Membranófonos'
        ));
        \DB::table('tipo_instrumentos')->insert(array(
        		'tipo' => 'Idiófonos'
        ));
        \DB::table('tipo_instrumentos')->insert(array(
        		'tipo' => 'Electrófonos'
        ));
    }
}
