<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Javier',
            'email' =>'javier@live.com',
            'password' => bcrypt('qwerty'),
            'tipo_user' => 'Admin',
            'status' => 'si',
        ]);
    }
}
