$(document).ready(function() {
	$('#alert').hide();
	$('#ancho_m').hide();
	$('#largo_m').hide();

	$("#tipo_m").on("change", function (event) {
		var id = event.target.value;

		$('#ancho_m').hide();
		$('#largo_m').hide();
		$('#separa').hide();
		$('#desc').hide();

		if(id == 1){
			$('#ancho_m').show();
			$('#largo_m').show();
			$('#separa').show().text('/');
			$('#desc').hide();
		}else if(id == 2){
			$('#ancho_m').show();
			$('#largo_m').hide();
			$('#separa').show().text('"');
			$('#desc').hide();
		}else if(id == 3){
			$('#ancho_m').show();
			$('#largo_m').show();
			$('#separa').show().text('x');
			$('#desc').show().text('cms');
		}else{
			$('#ancho_m').hide();
			$('#largo_m').hide();
			$('#separa').hide();
			$('#desc').hide();
		}
	});



    $("#padres").change(function() {
	    if(this.checked) {
	        $("#representantes").hide();
	        $("#aviso_r").hide();  
	    }else{
	        $("#representantes").show();
	        $("#aviso_r").show();
	    }
	});

	$("#vive_o_add").change(function() {
        if(this.checked) {
            $("#vive_otros_add").removeAttr('disabled');
        }else{
            $("#vive_otros_add").attr('disabled', true);
        }
        });


	$("#id_programa").on("change", function (event) {
    	var id = event.target.value;


    	$.get("/admin/subprograma/"+id+"/buscar",function (data) {
       
    	
	       $("#id_subPrograma").empty();
	       $("#id_subPrograma").append('<option value="" selected disabled> Seleccione el subprograma</option>');
	        var contar=0;
	        if(data.length > 0){

	            for (var i = 0; i < data.length ; i++) 
	            {  
	                $("#id_subPrograma").removeAttr('disabled');
	                $("#id_subPrograma").append('<option value="'+ data[i].id + '">' + data[i].sub_programa +'</option>');
	                if(data[i].id==8){
	                	contar++;
	                }
	            }
	            
	            
		    }else{
		            
		        $("#id_subPrograma").attr('disabled', false);

		    }
    	});
	});

	$("#id_subPrograma").on("change", function (event) {
    	var id = event.target.value;
    	$.get("/admin/materias/"+id+"/buscar",function (data) {
       
    	   	$("#vermaterias").empty();
	       	$('#vermaterias').append('<table><tr><td></td></tr></table>');
	        var contar=0;
	        if(data.length > 0){

	            for (var i = 0; i < data.length ; i++) 
	            {  
	                // $("#vermaterias").removeAttr('disabled');
	                $("#vermaterias").append(
	                	'<br><h4>Materias a visualizar</h4><br><table><tr><td>' + data[i].materia +'</td></tr></table><br>'
	                	);
	                if(data[i].id==8){
	                	contar++;
	                }
	            }
	            
		    }else{
		            
		        $("#vermaterias").append(
	                	'<br><h4>No hay materias en el subprograma</h4><br>'
	                	);

		    }
    	});
	});


	//LISTAR REGISTROS


	// $.get("/admin/periodos/buscar",function (data) {
       	    
 //       	    var contar=0;
	//         if(data.length > 0){

	//         	alert('trae');
	//             // for (var i = 0; i < data.length ; i++) 
	//             // {  
	//             //     $("#example1").append('<tr></td>'+ data[i].id + '</td><td>' + data[i].periodo +'</td></tr>');
	//             //     if(data[i].id==8){
	//             //     	contar++;
	//             //     }
	//             // }
	            
	            
	// 	    }else{
		            
	// 	    }
	// 	    	alert('no trae');
	// 	        // $("#example1").hide();

 //    	});
	
	//agregar registro
	$('.btn-store').click(function(e) {
		e.preventDefault();

		var form = 	$(this).parents('form');
		var url = 	form.attr('action');

		$('div.flash-message').show();
		$('#alert').show();
		
		// alert(nombre);					
		
		// alert(url+form.serialize());
		$.post(url, form.serialize(), function(result){
			// row.fadeOut();
			
			$('#total').html(result.total);
			$('div.flash-message').html(result.message).addClass("alert alert-success alert-block").delay(3000).fadeOut(350);
		}).fail(function(){
			
			$('#total').html(result.total);
			$('div.flash-message').html(result.message).addClass("alert alert-danger alert-block").delay(3000).fadeOut(350);
		});
	});















	//eliminar registro
	// $('.btn-delete').click(function(e){
	// 	e.preventDefault();
	// 	if (!  confirm("ELIMINAR REGISTRO?")) {
	// 		return false; //no pasa nada

	// 	}

	// 	var row = 	$(this).parents('tr');
	// 	var form = 	$(this).parents('form');
	// 	var url = 	form.attr('action');

	// 	$('div.flash-message').show();
	// 	$('#alert').show();

	// 	// alert(url+form.serialize());

	// 	$.post(url, form.serialize(), function(result){
	// 		row.fadeOut();
	// 		$('#num').html(result.num);
	// 		$('#total').html(result.total);
	// 		$('div.flash-message').html(result.message).addClass("alert alert-success alert-block").delay(3000).fadeOut(350);
	// 	}).fail(function(){
	// 		$('#num').html(result.num);
	// 		$('#total').html(result.total);
	// 		$('div.flash-message').html(result.message).addClass("alert alert-danger alert-block").delay(3000).fadeOut(350);
	// 	});

	// });



	//Traer datos de municipios a través del estado


	

	
	$("#id_estado").on("change", function (event) {
    	var id = event.target.value;


    	$.get("/admin/municipio/"+id+"/buscar",function (data) {
       
    	
	       $("#id_municipio").empty();
	       $("#id_municipio").append('<option value="" selected disabled> Seleccione el municipio</option>');
	        var contar=0;
	        if(data.length > 0){

	            for (var i = 0; i < data.length ; i++) 
	            {  
	                $("#id_municipio").removeAttr('disabled');
	                $("#id_municipio").append('<option value="'+ data[i].id + '">' + data[i].municipio +'</option>');
	                if(data[i].id==8){
	                	contar++;
	                }
	            }
	            
	            
		    }else{
		            
		        $("#id_municipio").attr('disabled', false);

		    }
    	});

    	$.get("/admin/ciudad/"+id+"/buscar",function (data) {
       
    	
	       $("#id_ciudad").empty();
	       $("#id_ciudad").append('<option value="" selected disabled> Seleccione la ciudad</option>');
	        var contar=0;
	        if(data.length > 0){

	            for (var i = 0; i < data.length ; i++) 
	            {  
	                $("#id_ciudad").removeAttr('disabled');
	                $("#id_ciudad").append('<option value="'+ data[i].id + '">' + data[i].ciudad +'</option>');
	                if(data[i].id==8){
	                	contar++;
	                }
	            }
	            
	            
		    }else{
		            
		        $("#id_ciudad").attr('disabled', false);

		    }
    	});

	});



	$('#id_municipio').on("change", function(event){
		var id = event.target.value;

		$.get("/admin/parroquia/"+id+"/buscar",function (data) {
       
    	
	       $("#id_parroquia").empty();
	       $("#id_parroquia").append('<option value="" selected disabled> Seleccione la parroquia</option>');
	        var contar=0;
	        if(data.length > 0){

	            for (var i = 0; i < data.length ; i++) 
	            {  
	                $("#id_parroquia").removeAttr('disabled');
	                $("#id_parroquia").append('<option value="'+ data[i].id + '">' + data[i].parroquia +'</option>');
	                if(data[i].id==8){
	                	contar++;
	                }
	            }
	            
	            
		    }else{
		            
		        $("#id_parroquia").attr('disabled', false);

		    }
    	});

	});
});