@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Auditoria - Listado
@endsection

@section('main-content')


<!-- Content Header (Page header) -->
@section('contentheader_title')
    Auditoria

    @section('contentheader_description') 
      Listado de la auditoria y acciones de los usuarios
    @endsection

    @section('contentheader_level') 
      Auditoria
    @endsection

    @section('contentheader_sublevel') 
      Listado
    @endsection




  @endsection
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
          <div class="flash-message"></div>
            @include('flash::message')
            <!-- <div style="border-radius: 30px;" id="alert" class="alert-success" align="center"></div> -->
            <br>
          <div class="panel panel-default">
            
            <div class="panel-heading">Lista de acciones de los usuarios

              

              
              

          </div>
              

              
              


          
          <div class="panel-body">
            <div class="box-body">

                
            <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Nro</th>
                <th>Acción</th>
                <th>Usuario</th>
                <th>Realizado el</th>
              </tr>
            </thead>
            <tbody>
              @foreach($auditoria as $key)
                <tr>
                  <td>{{$num=$num+1}}</td>
                  <td>{{$key->accion}}</td>
                  <td>{{$key->usuario->name}}</td>
                  <td>{{$key->created_at}}</td>
                </tr>
              @endforeach()
            </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

</div><!-- /.content-wrapper -->
</div>



<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>

@endsection

@section('script')
    <script src="{{ asset('js/scripts.js') }}"></script>
@endsection
