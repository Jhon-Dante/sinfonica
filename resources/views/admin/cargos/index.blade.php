@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Cargos - Listado
@endsection

@section('main-content')


<!-- Content Header (Page header) -->
@section('contentheader_title')
    Cargos

    @section('contentheader_description') 
      Listado de los cargos inscritos en el sistema
    @endsection

    @section('contentheader_level') 
      Cargos
    @endsection

    @section('contentheader_sublevel') 
      Listado
    @endsection




  @endsection
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
          <div class="flash-message"></div>
            @include('flash::message')
            <!-- <div style="border-radius: 30px;" id="alert" class="alert-success" align="center"></div> -->
            <br>
          <div class="panel panel-default">
            <div class="panel-heading">Lista de Cargos registrados - Total de Cargos: <span id="total"></span>

              

              <div class="btn-group pull-right" style="margin: 15px 0px 15px 15px;">
                <a href="#" data-toggle="modal" data-target="#myModal3" class="btn btn-success btn-flat" style="padding: 4px 10px; border-radius: 30px;">
                <i class="fa fa-pencil"></i> Registrar nuevo cargo   
                </a>
              </div>
              

          </div>

          
          <div class="panel-body">
            <div class="box-body">

                
            <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Nro</th>
                <th>Cargos</th>
                <th>Creado el</th>
                <th>status</th>
                <th>Opciones</th>
              </tr>
            </thead>
            <tbody>
              @foreach($cargos as $key)
                <tr>
                  <td>{{$num=$num+1}}</td>
                  <td>{{$key->cargo}}</td>
                  <td>{{$key->created_at}}</td>
                  <td>
                    @if($key->status == 'si')
                      <a href="{{ route('statusCargo', [$key->id]) }}">Si</a>
                    @else
                      <a href="{{ route('statusCargo', [$key->id]) }}">No</a>
                    @endif

                  </td>
                  <td>

                    <a href="#" onclick="eliminar('{{$key->id}}')" data-toggle="modal" data-target="#myModal" class="btn btn-danger btn-flat" style="padding: 4px 10px; border-radius: 50px;"><i class="fa fa-trash"></i>

                  </td>
                </tr>
              @endforeach()
            </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

</div><!-- /.content-wrapper -->

<div id="myModal3" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Registrar nuevo cargo</h4>
            </div>
            <div class="modal-body">
            
                
                {!! Form::open(['route' => ['registrarCargo'], 'method' => 'POST']) !!}
                
                    @include('admin.cargos.partials.create-fields')

                    <button class="btn btn-success btn-flat" title="Presionando este botón puede eliminar el registro" >Aceptar</button>
                {!! Form::close() !!}

                
            </div>
        </div>
        
    </div>
</div><!-- /.content-wrapper -->

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Eliminar cargo</h4>
            </div>
            <div class="modal-body">
            
                ¿Está realmente seguro de querer eliminar este cargo seleccionado?
                {!! Form::open(['route' => ['eliminarCargo'], 'method' => 'POST']) !!}
                    <input type="hidden" name="id" id="id">

                    <button class="btn btn-  btn-flat" title="Presionando este botón puede eliminar el registro" >Aceptar</button>
                {!! Form::close() !!}

                
            </div>
        </div>
        
    </div>
</div><!-- /.content-wrapper -->
</div>



<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
   <script type="text/javascript">

        function eliminar(id) {
          $('#id').val(id);
        }
    </script>
@endsection

@section('script')
    <script src="{{ asset('js/scripts.js') }}"></script>
@endsection
