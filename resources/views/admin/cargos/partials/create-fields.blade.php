<div class="row">
	<div class="col-md-12">
		<div class="form-group{{ $errors->has('cargo') ? ' has-error' : '' }}">
			{!! Form::text('cargo',null,['class' => 'form-control','placeholder' => 'Nombre del cargo', 'title' => 'Introduzca el cargo activo de las instituciones', 'id' => 'cargo', 'required']) !!}
		</div>
	</div>
</div>
