@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Estudiantes - Inscripción
@endsection

@section('main-content')


<!-- Content Header (Page header) -->
@section('contentheader_title')
    Estudiantes

    @section('contentheader_description') 
      Registro de estudiantes nuevos
    @endsection

    @section('contentheader_level') 
      Estudiantes
    @endsection

    @section('contentheader_sublevel') 
      Inscripción
    @endsection




  @endsection
<!-- Main content -->
<section class="content">
      <!-- Small boxes (Stat box) -->

      <div class="row">
         <div class="col-md-12">
                <!-- mensaje flash -->
                <div class="flash-message"></div>
                @include('flash::message')
      </div>
        {!! Form::open(['route' => ['inscripcionCreate'], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">

              <h3 class="box-title">Inscribir/Reinscribir estudiante</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <select class="form-control select2" style="width: 100%;" placeholder="Seleccionar estudiante a inscribir/Reinscribir" style="width: 100%;" name="id_datosBasicos">
                      @foreach($datosBasicos as $key)
                        <option value="{{$key->id}}">{{$key->nombres}},{{$key->nombres}} - {{$key->nacio}}.-{{$key->cedula}}</option>
                      @endforeach
                    </select>
                  </div>
                  <!-- /.form-group -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Inscribir</button>
            </div>
          </div>
        </div>
        {!! Form::close() !!}
        <!-- ./col -->
      </div>
      

      <div class="row">
       <div class="col-md-12">
        <div class="form-group">
          <div class="box box-default">
            <div class="box-header">
              <h3 class="box-title">¿El estudiante es mayor de edad?</h3>
            </div>
            <div class="box-body">
              <div class="form-group">
                <label>
                  <input type="checkbox" class="minimal" name="padres" id="padres">
                  ¿Mayor de edad?
                </label>
              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              Si el estudiante es mayor de edad, no será necesario registrar representantes
            </div>
          </div>
        </div>
       </div>
      </div>

      <div class="callout callout-info" id="aviso_r">
          <h4>¡Aviso!</h4>

          <p>Debe seleccionar el parentesco de los representantes en el mismo orden en la que seleccionó a los representantes</p>
      </div>
      {!! Form::open(['route' => ['prinscripcion'], 'method' => 'POST', 'enctype' => 'multipart/form-data', 'files' => 'true']) !!}


      <div class="row" id="representantes">
        <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Seleccionar representantes</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <select class="form-control select2" style="width: 100%;" data-placeholder="Seleccionar representantes del estudiante" style="width: 100%;" name="id_representante[]">
                      @foreach($representantes as $repre)
                        <option value="{{$repre->id}}">{{$repre->nombres}} {{$repre->apellidos}}, {{$repre->nacio}}.-{{$repre->cedula}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <select class="form-control select2" style="width: 100%;" data-placeholder="Seleccionar parentescos de los representantes del estudiante" style="width: 100%;" name="id_parentesco[]">
                      @foreach($parentesco as $key)
                        <option value="{{$key->id}}">{{$key->parentesco}}</option>
                      @endforeach
                    </select>
                  </div>
                  <!-- /.form-group -->
                  </div>
                </div>
                <!-- /.col -->
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <select class="form-control select2" style="width: 100%;" data-placeholder="Seleccionar representantes del estudiante" style="width: 100%;" name="id_representante[]">
                      @foreach($representantes as $repre)
                        <option value="{{$repre->id}}">{{$repre->nombres}} {{$repre->apellidos}}, {{$repre->nacio}}.-{{$repre->cedula}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <select class="form-control select2" style="width: 100%;" data-placeholder="Seleccionar parentescos de los representantes del estudiante" style="width: 100%;" name="id_parentesco[]">
                      @foreach($parentesco as $key)
                        <option value="{{$key->id}}">{{$key->parentesco}}</option>
                      @endforeach
                    </select>
                  </div>
                  <!-- /.form-group -->
                  </div>
                </div>
                <!-- /.col -->


              <!-- /.row -->
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                    ¿El representante no se encuentra Registrado?   
                    <a href="#" data-toggle="modal" data-target="#myModal3" class="btn btn-success btn-flat" style="padding: 4px 10px; border-radius: 30px;"><i class="fa fa-user-plus"></i></a>
            </div>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->

        <!-- Main row -->
        <div class="row">
          <div class="col-md-12">
            <div class="box box-warning">
              <div class="box-header with-border">
                <h3 class="box-title">Datos del estudiante</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-md-12">
                    
                    @include('admin.datosBasicos.partials.create-fields')
                    <!-- /.form-group -->
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                      
              </div>
            </div>
          </div>
          <!-- ./col -->
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Datos médicos del estudiante</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-md-12">

                    @include('admin.datosBasicos.partials.create-fields-medicos')
                    <!-- /.form-group -->
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                      
              </div>
            </div>
          </div>
          <!-- ./col -->
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="box box-danger">
              <div class="box-header with-border">
                <h3 class="box-title">Estudios generales del estudiante</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-md-12">
                    
                    @include('admin.datosBasicos.partials.create-fields-academicos')
                    <!-- /.form-group -->
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
              </div>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <div class="row">
          <div class="col-md-12">
            
            <button class="btn btn-warning pull-right" type="submit">Preinscribir</button>
          </div>
        </div>
        <!-- /.row (main row) -->

        {!! Form::close() !!}

    </section>
</section>



<div id="myModal3" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Registrar nuevo representante</h4>
            </div>
            <div class="modal-body">
            
                
                {!! Form::open(['route' => ['registrarRepresentante'], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}

                  @include('admin.representantes.partials.create-fields')


                
            </div>
            <div class="modal-footer">
              <button class="btn btn-success" type="submit">Registrar representante</button>
            </div>
                {!! Form::close() !!}
        </div>
        
    </div>
</div>




    <script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
   <script type="text/javascript">

        function mostrardatos(nombre,apellido,nacio,cedula,direccion,cod_telefono,telefono) 
        {
          // alert(nombre+apellido);
            $('#n').text(nombre);
            $('#a').text(apellido);
            $('#cedula').text(nacio+'.-'+cedula);
            $('#direccion').text(direccion);
            $('#telefono').text(0+cod_telefono+'-'+telefono);
        }

        function editardatos(nombre,apellido,nacio,cedula,direccion,cod_telefono,telefono) 
        {
            $('#n').text(nombre);
            $('#a').text(apellido);
            $('#cedula').text(nacio+'.-'+cedula);
            $('#direccion').text(direccion);
            $('#telefono').text(0+cod_telefono+'-'+telefono);
        }
    </script>
@endsection

@section('script')
    <script src="{{ asset('js/scripts.js') }}"></script>
@endsection
