@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Estudiantes - Listado
@endsection

@section('main-content')


<!-- Content Header (Page header) -->
@section('contentheader_title')
    Estudiantes

    @section('contentheader_description') 
      Listado de los estudiantes inscritos en el sistema
    @endsection

    @section('contentheader_level') 
      Estudiantes
    @endsection

    @section('contentheader_sublevel') 
      Listado
    @endsection




  @endsection
<!-- Main content -->
<section class="content">
    <div class="row">
      <div class="col-md-12">
                <!-- mensaje flash -->
                <div class="flash-message"></div>
                @include('flash::message')
      </div>
        <div class="col-xs-12">
            <!-- <div style="border-radius: 30px;" id="alert" class="alert-success" align="center"></div> -->
            <br>
          <div class="panel panel-default">
            <div class="panel-heading">Lista de estudiantes registrados - Total de estudiantes: <span id="total"></span>

              

              <div class="btn-group pull-right" style="margin: 15px 0px 15px 15px;">
                <a href="{{ url('admin/datosBasicos/create') }}" class="btn btn-success btn-flat" style="padding: 4px 10px; border-radius: 30px;">
                <i class="fa fa-pencil"></i> Registrar nuevo estudiante   
                </a>
              </div>
              

          </div>

          
          <div class="panel-body">
            <div class="box-body">

                
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Nro</th>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Cédula/passport</th>
                    <th>Programa</th>
                    <th>Registrado</th>
                    <th>modificado</th>
                    <th>Status</th>
                    <th>Foto</th>
                    <th>Opciones</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($inscripcion as $key)
                    <tr>
                      <td>{{$num=$num+1}}</td>
                      <td>{{$key->datoBasico->nombres}}</td>
                      <td>{{$key->datoBasico->apellidos}}</td>
                      <td>{{$key->datoBasico->nacio}}.-{{$key->datoBasico->cedula}}</td>
                      <td>{{$key->sub_programa->programa->programa}} -<br>{{$key->sub_programa->sub_programa}}</td>
                      <td>{{$key->datoBasico->created_at}}</td>
                      <td>{{$key->datoBasico->updated_at}}</td>
                      <td>{{$key->datoBasico->status}}</td>
                      <td>
                        
                      <td>
                        @foreach($datosBasicos_m as $medicos)
                        @foreach($datosBasicos_e as $estudios)
                          @if($key->datoBasico->id==$medicos->datoBasico->id && $key->datoBasico->id==$estudios->datoBasico->id)

                          <div class="input-group">
                            <div class="input-group-btn">
                              <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Acción
                                <span class="fa fa-caret-down"></span></button>
                              <ul class="dropdown-menu">
                                <li><a href="#" onclick="mostrardatos(
                                '{{$key->datoBasico->nombres}}',
                                '{{$key->datoBasico->apellidos}}',
                                '{{$key->datoBasico->nacio}}',
                                '{{$key->datoBasico->cedula}}',
                                '{{$key->datoBasico->lugar_nac}}',
                                '{{$key->datoBasico->fecha_nac}}',
                                '{{$key->datoBasico->genero}}',
                                '{{$key->datoBasico->vive_p}}',
                                '{{$key->datoBasico->vive_m}}',
                                '{{$key->datoBasico->vive_o}}',
                                '{{$key->datoBasico->vive_otros}}',
                                '{{$key->datoBasico->direccion}}',
                                '{{$key->datoBasico->cod_tel}}',
                                '{{$key->datoBasico->telefono}}',
                                '{{$key->datoBasico->status}}',

                                '{{$estudios->institucion}}',
                                '{{$estudios->tipo}}',
                                '{{$estudios->direccion}}',
                                '{{$estudios->nombre_director}}',
                                '{{$estudios->grado}}',
                                '{{$estudios->turno}}',

                                '{{$medicos->v_triple}}',
                                '{{$medicos->v_polio}}',
                                '{{$medicos->v_bcg}}',
                                '{{$medicos->v_antirabiolica}}',
                                '{{$medicos->v_rubeola}}',
                                '{{$medicos->v_sarampion}}',
                                '{{$medicos->v_meningitis}}',
                                '{{$medicos->v_hepatitis}}',
                                '{{$medicos->otros}}',
                                '{{$medicos->f_sangre}}',
                                '{{$medicos->padecimiento}}',
                                '{{$medicos->tratamiento}}',
                                '{{$medicos->medico_tratante}}',
                                '{{$medicos->alergias}}',
                                '{{$medicos->tratamiento2}}'

                                )" data-toggle="modal" data-target="#myModal2" class="btn btn-default btn-flat">
                                <i class="fa fa-eye"> Ver datos</i>
                            </a></li>
                                <!-- <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li> -->
                                <li class="divider"></li>
                                <div align="center">Constancias</div>
                                @if(Auth::user()->const_estu == 'Si')
                                    <li><a href="{{ url('admin/constanciaI/'.$key->id) }}"><i class='fa fa-book'></i>inscripción</a></li>
                                @endif
                                <li><a href="{{ url('admin/constanciaAudicion/'.$key->id) }}"><i class='fa fa-book'></i>Audicion</a></li>
                                <li><a href="#" onclick="CPermiso(
                                    '{{$key->id}}',
                                    '{{$key->datoBasico->nombres}}',
                                    '{{$key->datoBasico->apellidos}}'
                                )" data-toggle="modal" data-target="#myModal"><i class='fa fa-book'></i>Permiso</a></li>
                              </ul>
                            </div>
                            <!-- /btn-group -->
                          </div>
                            
                          @endif
                        @endforeach
                      @endforeach
                         
                      </td>
                    </tr>
                  @endforeach
                    
                
                </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

</div><!-- /.content-wrapper -->

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Especifique datos de la constancia para el estudiante: <p id="n"><span></span></p></h4>
            </div>
            {!! Form::open(['route' => ['constanciaPermiso'], 'method' => 'GET', 'enctype' => 'multipart/form-data']) !!}
                <div class="modal-body">
                    {!! Form::label('dia','Especifique dia') !!}
                    <div class="form-group">
                        <input type="date" name="dia" class="form-control">
                    </div>
                    {!! Form::label('hora','Especifique hora') !!}
                    <div class="form-group">
                        <input type="time" name="hora" class="form-control">
                    </div>
                    {!! Form::label('motivo','Especifique el motivo') !!}
                    <div class="form-group">
                        <textarea name="motivo" placeholder="motivo" class="form-control"></textarea>
                    </div>
                    <input type="hidden" name="id" id="id">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Aceptar</button>
                </div>
            {!! Form::close() !!}
        </div>
        
    </div>
</div><!-- /.content-wrapper -->


<div id="myModal2"  class="modal fade" role="dialog">
  <div class="modal-dialog">
            <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Datos del estudiante</h4>
      </div>
      <div class="modal-body">               
        <strong>Fecha de la audición:</strong>
        <p id="audicion"><span></span></p>
        <br>
        <strong>Nombres: </strong>
        <p id="n"><span></span></p>
        <br>
        <strong>Apellidos: </strong>
        <p id="a"><span></span></p>
        <br>
        <strong>Cédula: </strong> 
        <p id="cedula"><span></span></p>
        <br>
        <strong>Lugar de Nacimiento</strong>
        <p id="lugar_nac"><span></span></p>
        <br>
        <strong>Fecha de Nacimiento</strong>
        <p id="fecha_nac"><span></span></p>
        <br>
        <strong>Género</strong>
        <p id="genero"><span></span></p>
        <br>
        <strong>¿Vive con el padre?</strong>
        <p id="vive_p"><span></span></p>
        <br>
        <strong>¿Vive con la madre?</strong>
        <p id="vive_m"><span></span></p>
        <br>
        <strong>¿Vive con otros?</strong>
        <p id="vive_o"><span></span></p>
        <br>
        <strong>¿Quienes otros?</strong>
        <p id="vive_otros"><span></span></p>
        <br>
        <strong>Direccion: </strong>
        <p id="direccion"><span></span></p>
        <br>
        <strong>telefono: </strong> 
        <p id="telefono"><span></span></p>
        <hr>

        <h3>Datos Acémicos</h3>

        <strong>Institución: </strong> 
        <p id="institucion"><span></span></p>
        <br>
        <strong>Tipo: </strong> 
        <p id="tipo"><span></span></p>
        <br>
        <strong>Dirección del plantel: </strong> 
        <p id="direccion_p"><span></span></p>
        <br>
        <strong>Nombre del director: </strong> 
        <p id="nombre_director"><span></span></p>
        <br>
        <strong>Grado: </strong> 
        <p id="grado"><span></span></p>
        <br>
        <strong>Turno: </strong> 
        <p id="turno"><span></span></p>
        <br>

        <h3>Datos Médicos</h3>

        <strong>Vacuna: Triple </strong> 
        <p id="v_triple"><span></span></p>
        <br>
        <strong>Vacuna: Polio </strong> 
        <p id="v_polio"><span></span></p>
        <br>
        <strong>Vacuna: BCG </strong> 
        <p id="v_bcg"><span></span></p>
        <br>
        <strong>Vacuna: Antirabiólica </strong> 
        <p id="v_antirabiolica"><span></span></p>
        <br>
        <strong>Vacuna: Rubeola </strong> 
        <p id="v_rubeola"><span></span></p>
        <br>
        <strong>Vacuna: Sarampion </strong> 
        <p id="v_sarampion"><span></span></p>
        <br>
        <strong>Vacuna: Meningitis </strong> 
        <p id="v_meningitis"><span></span></p>
        <br>
        <strong>Vacuna: Hepatitis </strong> 
        <p id="v_hepatitis"><span></span></p>
        <br>
        <strong>Otras vacunas </strong> 
        <p id="otros"><span></span></p>
        <br>
        <strong>Tipo de sangre</strong> 
        <p id="f_sangre"><span></span></p>
        <br>
        <strong>Padecimientos</strong> 
        <p id="padecimiento"><span></span></p>
        <br>
        <strong>Tratamiento</strong> 
        <p id="tratamiento"><span></span></p>
        <br>
        <strong>Médico tratante</strong> 
        <p id="medico_tratante"><span></span></p>
        <br>
        <strong>Alergias</strong> 
        <p id="alergias"><span></span></p>
        <br>
        <strong>Tratamiento</strong> 
        <p id="tratamiento2"><span></span></p>
        <br>

        </div>
      <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div id="myModal4" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Editar estudiante</h4>
        </div>
        <div class="modal-body">
            

                @include('admin.datosBasicos.partials.edit-fields')

            
        
        </div>
         </div>
       </div>
     </div>
   </div>

</div>

    <script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
   <script type="text/javascript">

        function mostrardatos(nombres,apellidos,nacio,cedula,lugar_nac,fecha_nac,genero,vive_p,vive_m,vive_o,vive_otros,direccion,cod_tel,telefono,status,institucion,tipo,direccion_p,nombre_director,grado,turno,v_triple,v_polio,v_bcg,v_antirabiolica,v_rubeola,v_sarampion,v_meningitis,v_hepatitis,otros,f_sangre,padecimiento,tratamiento,medico_tratante,alergias,tratamiento2) 
        {
          // alert(nombre+apellido);
            $('#n').text(nombres);
            $('#a').text(apellidos);
            $('#cedula').text(nacio+'.-'+cedula);
            $('#lugar_nac').text(lugar_nac);
            $('#fecha_nac').text(fecha_nac);
            $('#genero').text(genero);
            $('#vive_p').text(vive_p);
            $('#vive_m').text(vive_m);
            $('#vive_o').text(vive_o);
            $('#vive_otros').text(vive_otros);
            $('#direccion').text(direccion);
            $('#telefono').text(0+cod_tel+'-'+telefono);

            $('#institucion').text(institucion);
            $('#tipo').text(tipo);
            $('#direccion_p').text(direccion_p);
            $('#nombre_director').text(nombre_director);
            $('#grado').text(grado);
            $('#turno').text(turno);

            $('#v_triple').text(v_triple);
            $('#v_polio').text(v_polio);
            $('#v_bcg').text(v_bcg);
            $('#v_antirabiolica').text(v_antirabiolica);
            $('#v_rubeola').text(v_rubeola);
            $('#v_sarampion').text(v_sarampion);
            $('#v_meningitis').text(v_meningitis);
            $('#v_hepatitis').text(v_hepatitis);
            $('#otros').text(otros);
            $('#f_sangre').text(f_sangre);
            $('#padecimiento').text(padecimiento);
            $('#tratamiento').text(tratamiento);
            $('#medico_tratante').text(medico_tratante);
            $('#alergias').text(alergias);
            $('#tratamiento2').text(tratamiento2);

        }

        function editardatos(nombre,apellido,nacio,cedula,direccion,cod_telefono,telefono) 
        {
            $('#n').text(nombre);
            $('#a').text(apellido);
            $('#cedula').text(nacio+'.-'+cedula);
            $('#direccion').text(direccion);
            $('#telefono').text(0+cod_telefono+'-'+telefono);
        }

        function CPermiso(id, nombre, apellido){
            $('#id').val(id);
            $('#n').text(nombre+' '+apellido);
        }
    </script>
@endsection

@section('script')
    <script src="{{ asset('js/scripts.js') }}"></script>
@endsection
