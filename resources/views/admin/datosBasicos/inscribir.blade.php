@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Estudiante - Inscribir
@endsection

@section('main-content')


<!-- Content Header (Page header) -->
@section('contentheader_title')
    Estudiantes

    @section('contentheader_description') 
      Incribiendo/Reinsbribiendo al estudiante {{$estudiante->nombres}}, {{$estudiante->apellidos}}
    @endsection

    @section('contentheader_level') 
      Estudiantes
    @endsection

    @section('contentheader_sublevel') 
      Inscribir
    @endsection




  @endsection
<!-- Main content -->
<section class="content">
    <div class="row">
      <div class="col-md-12">
                <!-- mensaje flash -->
                <div class="flash-message"></div>
                @include('flash::message')
      </div>
        <div class="col-xs-12">
            <!-- <div style="border-radius: 30px;" id="alert" class="alert-success" align="center"></div> -->
            <br>
          <div class="panel panel-default">
            <div class="panel-heading">Inscribiendo al estudiante <strong>{{$estudiante->nombres}}, {{$estudiante->apellidos}}</strong>. De cédula/Passaporte: <strong>{{$estudiante->nacio}}.-{{$estudiante->cedula}}</strong>
          </div>

          {!! Form::open(['route' => ['reinscripcion'], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
            <div class="panel-body">
              <div class="box-body">
  				      @include('admin.datosBasicos.partials.inscribir-fields')
            	</div>
            </div>

          <div class="panel-footer">
          <input type="hidden" name="id_datosBasicos" value="{{$estudiante->id}}">
        <button class="btn btn-success">Inscribir</button>
          </div>
          {!! Form::close() !!}
      </div>
    </div>
  </div>
</section>

</div><!-- /.content-wrapper -->






   
@endsection

@section('script')
    <script src="{{ asset('js/scripts.js') }}"></script>
@endsection
