<div class="row">
	<div class="col-md-1">
		<div class="form-group">
            <label>Pública
              <input type="radio" name="tipo" class="minimal" checked value="publica">
            </label>
         </div>
    </div>
    <div class="col-md-1">
    	<div class="form-group">
            <label>Privada
              <input type="radio" name="tipo" class="minimal" value="privada">
            </label>
    	</div>
	</div>

	<div class="col-md-5">
		{!! Form::text('nombre',null,['class' => 'form-control', 'placeholder' => 'Nombre de la institución','required' => 'required']) !!}
	</div>
	<div class="col-md-5">
		{!! Form::text('director',null,['class' => 'form-control', 'placeholder' => 'Director del plantel','required' => 'required']) !!}
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		{!! Form::textarea('direccion',null,['class' => 'form-control', 'placeholder'=> 'Dirección del plantel','required' => 'required']) !!}
	</div>
</div>

<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			<label>Curso</label>
          <select class="form-control select2" required name="grado">
            <option selected>1er grado</option>
            <option>2do grado</option>
            <option>3er grado</option>
            <option>4to grado</option>
            <option>5to grado</option>
            <option>6to grado</option>
            <option>1er año</option>
            <option>2do año</option>
            <option>3er año</option>
            <option>4to año</option>
            <option>5to año</option>
            <option>6to año</option>  
            <option>Universitario</option>
          </select>
        </div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label>Turno</label>
          <select class="form-control select2" name="turno" required>
            <option value="mañana">Mañana</option>
            <option value="tarde">Tarde</option>
            <option value="noche">Noche</option>
          </select>
        </div>
	</div>
</div>