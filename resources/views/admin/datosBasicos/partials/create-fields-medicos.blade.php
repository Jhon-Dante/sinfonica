	{!! Form::label('vacunas','Vacunas') !!}<br>
<div class="row">
	<div class="col-md-2">
			<div class="form-group">
				<div class="checkbox">
				    <label>
				     	<input type="checkbox" name="v_triple" value="si">
				     	Triple
				    </label>
				 </div>
			</div>
	</div>
	<div class="col-md-2">
		<div class="form-group">
			<div class="checkbox">
			    <label>
			     	<input type="checkbox" name="v_polio" value="si">
			     	Polio
			    </label>
			 </div>
		</div>
	</div>
	<div class="col-md-2">
		<div class="form-group">
			<div class="checkbox">
			    <label>
			     	<input type="checkbox" name="v_bcg" value="si">
			     	B.C.G.
			    </label>
			 </div>
		</div>
	</div>
	<div class="col-md-2">
		<div class="form-group">
			<div class="checkbox">
			    <label>
			     	<input type="checkbox" name="v_antiraviolica" value="si">
			     	Antiraviólica
			    </label>
			 </div>
		</div>
	</div>
	<div class="col-md-2">
		<div class="form-group">
			<div class="checkbox">
			    <label>
			     	<input type="checkbox" name="v_rubeola" value="si">
			     	Rubéola
			    </label>
			 </div>
		</div>
	</div>
	<div class="col-md-2">
		<div class="form-group">
			<div class="checkbox">
			    <label>
			     	<input type="checkbox" name="v_sarampion" value="si">
			     	Sarampión
			    </label>
			 </div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-2">
		<div class="form-group">
			<div class="checkbox">
			    <label>
			     	<input type="checkbox" name="v_meningitis" value="si">
			     	Meningitis
			    </label>
			 </div>
		</div>
	</div>
	<div class="col-md-2">
		<div class="form-group">
			<div class="checkbox">
			    <label>
			     	<input type="checkbox" name="v_hepatitis" value="si">
			     	Hepatitis
			    </label>
			 </div>
		</div>
	</div>
	<div class="col-md-8">
		<div class="form-group">
			<input class="form-control" name="otros" type="text" multiple="" placeholder="¿Alguna otra vacuna?">
		</div>
	</div>
</div>

<hr>
Grupo sanguíneo
<div class="row">
	<div class="col-md-4">
		<div class="form-group">
          <select class="form-control" data-placeholder="Grupo sanguíneo" name="g_sanguineo">
			<option value="AB+">AB+</option>
			<option value="AB-">AB-</option>
			<option value="A+">A+</option>
			<option value="A-">A-</option>
			<option value="B+">B+</option>
			<option value="B-">B-</option>
			<option value="O+">O+</option>
			<option value="O-">O-</option>
			<option value="RH">RH</option>
          </select>
        </div>
	</div>
	<div class="col-md-4">
		{!! Form::text('padecimiento',null,['class' => 'form-control','placeholder' => 'Padecimiento', 'title' => 'Ingrese los Nombres del estudiante', 'style'=>$errors->has('padecimiento') ? 'border-color: red; border: 1px solid red;': '']) !!}
	</div>
	<div class="col-md-4">
		{!! Form::text('tratamiento',null,['class' => 'form-control','placeholder' => 'Tratamientos', 'title' => 'Ingrese los Nombres del estudiante', 'style'=>$errors->has('tratamiento') ? 'border-color: red; border: 1px solid red;': '']) !!}
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-4">
		{!! Form::text('medico_tratante',null,['class' => 'form-control','placeholder' => 'Médico Tratante', 'title' => 'Ingrese los Nombres del estudiante', 'style'=>$errors->has('medico_tratante') ? 'border-color: red; border: 1px solid red;': '']) !!}
	</div>
	<div class="col-md-4">
		{!! Form::text('alergias',null,['class' => 'form-control','placeholder' => 'Alergias', 'title' => 'Ingrese los Nombres del estudiante', 'style'=>$errors->has('alergias') ? 'border-color: red; border: 1px solid red;': '']) !!}
	</div>
	<div class="col-md-4">
		{!! Form::text('tratamiento2',null,['class' => 'form-control','placeholder' => 'Tratamiento', 'title' => 'Ingrese los Nombres del estudiante', 'style'=>$errors->has('tratamiento2') ? 'border-color: red; border: 1px solid red;': '']) !!}
	</div>
</div>