<div class="row">
	<div class="col-md-12">
			{!! Form::label('foto_add','Foto del estudiante')!!}
			{!! Form::file('foto_add',null,['class' => 'form-control','placeholder' => 'foto del estudiante', 'title' => 'Introduzca el nombre del estudiante', 'id' => 'foto_add','required' => 'required']) !!}
	</div>
</div>

<hr>

<div class="row">
	<div class="col-md-6">
		<div class="form-group{{ $errors->has('nombre_add') ? ' has-error' : '' }}">
			{!! Form::text('nombres_add',null,['class' => 'form-control','placeholder' => 'Nombres del estudiante', 'title' => 'Introduzca los nombres del estudiante', 'id' => 'nombres_add','required' => 'required']) !!}
		</div>
	</div>

	<div class="col-md-6">
		<div class="form-group{{ $errors->has('rif_add') ? ' has-error' : '' }}">
			{!! Form::text('apellidos_add',null,['class' => 'form-control','placeholder' => 'Apellidos del estudiante', 'title' => 'Ingrese los apellidos del estudiante','id' => 'apellidos_add','required' => 'required']) !!}
		</div>
	</div>
</div>

{!! Form::label('nacio_add','Cédula/passaporte del estudiante') !!}
<div class="row">
	<div class="col-md-4">
		<div class="form-group{{ $errors->has('nombre_add') ? ' has-error' : '' }}">
			{!! Form::select('nacio_add',['V' => 'V', 'E' => 'E'],null,['class' => 'form-control','title' => 'Ingrese la nacionalidad del estudiante','min' => '6','maxLength' => '7','oninput' => 'javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);','id' => 'telefono_opc_add','required' => 'required']) !!}
		</div>
	</div>

	<div class="col-md-8">
		<div class="form-group{{ $errors->has('cedula_add') ? ' has-error' : '' }}">
			{!! Form::number('cedula_add',null,['class' => 'form-control','placeholder' => 'Cédula', 'title' => 'Ingrese la cédula del personal','min' => '7','maxLength' => '7','oninput' => 'javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);','id' => 'cedula_add','required' => 'required']) !!}
		</div>
	</div>
</div>

<hr>


<div class="row">
	<div class="col-md-6">
		<div class="form-group{{ $errors->has('lugar_nac_add') ? ' has-error' : '' }}">
			{!! Form::text('lugar_nac_add',null,['class' => 'form-control','placeholder' => 'Lugar de nacimiento', 'title' => 'Introduzca el lugar de nacimiento del estudiante', 'id' => 'lugar_nac_add','required' => 'required']) !!}
		</div>
	</div>

	<div class="col-md-6">
		<div class="form-group{{ $errors->has('fecha_nac_add') ? ' has-error' : '' }}">
			{!! Form::date('fecha_nac_add',null,['class' => 'form-control','placeholder' => 'Fecha de nacimiento', 'title' => 'Ingrese la fecha de nacimiento del estudiante','id' => 'fecha_nac_add','required' => 'required']) !!}
		</div>
	</div>
</div>

<hr>
<strong>Género</strong>
<div class="row">
	<div class="col-md-2">
		<div class="form-group">
            <label>Masculino
              <input type="radio" name="sexo_add" class="minimal" checked value="m">
            </label>
         </div>
    </div>
    <div class="col-md-2">
    	<div class="form-group">
            <label>Femenino
              <input type="radio" name="sexo_add" class="minimal" value="f">
            </label>
    	</div>
	</div>
	<div class="col-md-2">
    	<div class="form-group">
            <label>Otros
              <input type="radio" name="sexo_add" class="minimal" value="otros">
            </label>
    	</div>
	</div>
	
	<div class="col-md-1">
		<div class="form-group">
		    <label>Vive con padre
		      <input type="checkbox" class="minimal" name="vive_p_add" value="si">
		    </label>
		</div>
	</div>
	<div class="col-md-1">
		<div class="form-group">
		    <label>Vive con madre
		      <input type="checkbox" class="minimal" name="vive_m_add" value="si">
		    </label>
		</div>
	</div>
	<div class="col-md-1">
		<div class="form-group">
		    <label>Vive con otra persona
		      <input type="checkbox" class="minimal" name="vive_o_add" value="si" id="vive_o_add">
		    </label>
		</div>
	</div>
	<div class="col-md-3">
		<input type="text" class="form-control" name="vive_otros_add" title="Especifique la otra persona con quien vive el estudiante" placeholder="Especifique la otra persona con quien vive el estudiante" id="vive_otros_add" disabled>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="form-group{{ $errors->has('direccion_add') ? ' has-error' : '' }}">
			{!! Form::textarea('direccion_add',null,['class' => 'form-control','placeholder' => 'Dirección del estudiante', 'title' => 'Ingrese la dirección de la institución', 'id' => 'direccion_add','required' => 'required']) !!}
		</div>
	</div>
</div>

<hr>

{!! Form::label('cod_tel_add','Teléfono del estudiante') !!}
<div class="row">
	<div class="col-md-4">
		<div class="form-group{{ $errors->has('cod_tel_add') ? ' has-error' : '' }}">

			{!! Form::select('cod_tel_add',[
			'0244' => '0244', 
			'0424' => '0424',
			'0416' => '0416',
			'0426' => '0426'
			],null,['class' => 'form-control', 'title' => 'Seleccione la nacionalidad del personal', 'maxlength','7','id' => 'cod_tel_add','required' => 'required']) !!}
		</div>
	</div>
	<div class="col-md-8">
		<div class="form-group{{ $errors->has('telefono') ? ' has-error' : '' }}">
			{!! Form::number('telefono',null,['class' => 'form-control','placeholder' => 'Teléfono', 'title' => 'Ingrese el teléfono móvil del personal','title' => 'Ingrese la cedula del personal','min' => '7','maxLength' => '7','oninput' => 'javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);','id' => 'telefono','required' => 'required']) !!}
		</div>
	</div>
</div>














<!-- <div class="row">
	<div class="col-md-6">
		<div class="form-group{{ $errors->has('cedula') ? ' has-error' : '' }}">

				{!! Form::label('cedula','Cédula') !!}<br>
			<div class="row">
				<div class="col-md-2">
					 <select class="form-control" name="nacio">
			            <option value="V">V</option>
						<option value="E">E</option>
			          </select>
								
				</div>
				<div class="col-xs-10">
					
					{!! Form::number('cedula',null,['class' => 'form-control','placeholder' => 'Ej: 24999000', 'title' => 'Ingrese la cedula del estudiante','maxlength' => '8', 'style'=>$errors->has('cedula') ? 'border-color: red; border: 1px solid red;': '','oninput' => 'javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);','required' => 'required']) !!}
				</div>
			</div>
			
		</div>

		<div class="form-group{{ $errors->has('nombres') ? ' has-error' : '' }}">
			{!! Form::label('nombres','Nombres') !!}
			{!! Form::text('nombres',null,['class' => 'form-control','placeholder' => 'Ej: Juan José', 'title' => 'Ingrese los Nombres del estudiante', 'style'=>$errors->has('nombres') ? 'border-color: red; border: 1px solid red;': '','required' => 'required']) !!}
		</div>
		
		<div class="form-group{{ $errors->has('apellidos') ? ' has-error' : '' }}">
			{!! Form::label('apellidos','Apellidos') !!}
			{!! Form::text('apellidos',null,['class' => 'form-control','placeholder' => 'Ej: Ramírez Zerpa', 'title' => 'Ingrese el primer y segundo apellido del estudiante', 'style'=>$errors->has('apellidos') ? 'border-color: red; border: 1px solid red;': '','required' => 'required']) !!}
		</div>
		<div class="form-group{{ $errors->has('lugar_nac') ? ' has-error' : '' }}">
			{!! Form::label('lugar_nac','Lugar de nacimiento') !!}
			{!! Form::text('lugar_nac',null,['class' => 'form-control','placeholder' => 'Ej: Hospital de clínicas Aragua', 'title' => 'Ingrese el lugar de nacimiento del estudiante','required' => 'required']) !!}
		</div>
		<div class="form-group{{ $errors->has('fecha_nac') ? ' has-error' : '' }}">
			{!! Form::label('fecha_nac','Fecha de nacimiento') !!}
			{!! Form::date('fecha_nac',null,['class' => 'form-control','placeholder' => 'Fecha de nacimiento', 'title' => 'Ingrese la fecha de nacimiento del estudiante','required' => 'required']) !!}
		</div>
		</div>

		<div class="col-md-6">
		<div class="form-group{{ $errors->has('sexo') ? ' has-error' : '' }}">
			{!! Form::label('sexo','Género:') !!}
			{!! Form::label('sexo','M') !!}
			{!! Form::radio('sexo','M',['title' => 'Seleccione si es de Género Masculino','required' => 'required']) !!}
			{!! Form::label('sexo','F') !!}
			{!! Form::radio('sexo','F',['title' => 'Seleccione si es de Género Femenino','required' => 'required']) !!}
		</div>
		<div class="form-group{{ $errors->has('peso') ? ' has-error' : '' }}">
			{!! Form::label('peso','Peso') !!}
			{!! Form::text('peso',null,['class' => 'form-control','placeholder' => 'Ej: 23', 'title' => 'Ingrese el peso del estudiante','required' => 'required']) !!}
		</div>
		<div class="form-group{{ $errors->has('talla') ? ' has-error' : '' }}">
			{!! Form::label('talla','Talla') !!}
			{!! Form::text('talla',null,['class' => 'form-control','placeholder' => 'Ej: 4 - L', 'title' => 'Ingrese la talla del estudiante','required' => 'required']) !!}
		</div>
		<div class="form-group{{ $errors->has('salud') ? ' has-error' : '' }}">
			{!! Form::label('salud','Salud') !!}
			{!! Form::text('salud',null,['class' => 'form-control','placeholder' => 'Ej: Autismo, ceguera...', 'title' => 'Ingrese las enfermedades o discapacidades que padece el estudiante','required' => 'required']) !!}
		</div>
		<div class="form-group{{ $errors->has('direccion') ? ' has-error' : '' }}">
			{!! Form::label('direccion','Dirección') !!}
			{!! Form::textarea('direccion',null,['class' => 'form-control','placeholder' => 'Dirección', 'title' => 'Ingrese la dirección del estudiante','rows' => '3','required' => 'required']) !!}
		</div>
	</div>
</div>





 -->