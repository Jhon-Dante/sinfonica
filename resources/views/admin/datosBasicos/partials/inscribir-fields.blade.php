{!! Form::label('audi_fecha','Fecha de la audición') !!}
<div class="row">
	<div class="col-md-6">
		<div class="form-group{{ $errors->has('audi_fecha') ? ' has-error' : '' }}">
			{!! Form::date('audi_fecha',null,['class' => 'form-control','placeholder' => 'Fecha de audición', 'title' => 'Introduzca la fecha de audición del estudiante', 'id' => 'audi_fecha','required' => 'required']) !!}
		</div>
	</div>

	<div class="col-md-6">
		<div class="form-group{{ $errors->has('rif_add') ? ' has-error' : '' }}">
			<select name="id_profesor" class="form-control select2" style="width: 100%;" id="id_profesor" title="Seleccione el profesor que evaluó la audición" required>
				<option disabled>Seleccione el profesor que evaluó la audición</option>
				@foreach($personal as $perso)
					<option value="{{$perso->id}}">{{$perso->nombre}} {{$perso->apellidos}} - {{$perso->nacio}}.{{$perso->cedula}}</option>
				@endforeach()
			</select>
		</div>
	</div>
</div>
<hr>
{!! Form::label('id_periodo','Periodo a inscribir') !!}
<div class="row">
	<div class="col-md-12">
		<div class="form-group{{ $errors->has('id_periodo') ? ' has-error' : '' }}">
			{!! Form::text('id_periodo',$periodo->periodo,['class' => 'form-control','title' => 'Ingrese la nacionalidad del estudiante','min' => '6','maxLength' => '7','oninput' => 'javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);','id' => 'id_periodo','required' => 'required','disabled' => 'disabled']) !!}
			<input type="hidden" name="id_periodo" value="{{$periodo->id}}">
		</div>
	</div>
</div>

<hr>

{!! Form::label('id_programa','Programa a inscribir al estudiante') !!}
<div class="row">
	<div class="col-md-6">
		<div class="form-group{{ $errors->has('id_programa') ? ' has-error' : '' }}">
			<select name="id_programa" id="id_programa" class="form-control select2" style="width: 100%;" placeholder="Seleccionar estudiante a inscribir/Reinscribir" style="width: 100%;">
				@foreach($programas as $programa)
					<option value="{{$programa->id}}">{{$programa->programa}}</option>
				@endforeach()
			</select>
		</div>
	</div>

	<div class="col-md-6">
		<div class="form-group{{ $errors->has('id_subPrograma') ? ' has-error' : '' }}">
			{!! Form::select('id_subPrograma',['placeholder' => 'Sub-programa'],null,['class' => 'form-control','required' => 'required', 'title' => 'Seleccione el sub-programa de la materia','style' => 'width: 100%','id' => 'id_subPrograma','disabled' => 'disabled','required' => 'required']) !!}
		</div>
	</div>

	<!-- <div class="col-md-6">
		<div class="form-group{{ $errors->has('id_materias') ? ' has-error' : '' }}">
			{!! Form::select('id_materias',['placeholder' => 'Sub-programa'],null,['class' => 'form-control','required' => 'required', 'title' => 'Seleccione el sub-programa de la materia','style' => 'width: 100%','id' => 'id_materias','disabled' => 'disabled','required' => 'required']) !!}
		</div>
	</div> -->
</div>

<div class="row">
	<div class="col-md-12">
		<div id="vermaterias">
			
		</div>
	</div>
</div>
<hr>

<div class="row">
	<div class="col-md-8">
		<div class="form-group{{ $errors->has('id_instrumento') ? ' has-error' : '' }}">
			{!! Form::label('id_instrumento','Instrumento') !!}
			<select class="form-control select2" style="width: 100%;" placeholder="Seleccionar estudiante a inscribir/Reinscribir" style="width: 100%;" name="id_instrumento">
              @foreach($instrumentos as $instru)
                <option value="{{$instru->id}}"><strong>Nombre </strong>{{$instru->nombre}}-<strong> Tipo </strong>{{$instru->tipo}} - <strong> serial </strong>{{$instru->serial}} - <strong> Inventario </strong>{{$instru->inventario}}</option>
              @endforeach
            </select>
		</div>
	</div>
	{!! Form::label('instrumento','¿El estudiante trae su instrumento?') !!}
	<div class="col-md-4">
		<div class="form-group">
		      <input type="checkbox" class="minimal" name="instrumento" value="si">
		</div>
	</div>
</div>

