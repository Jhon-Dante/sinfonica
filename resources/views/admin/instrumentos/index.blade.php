@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Instrumentos - Listado
@endsection

@section('main-content')


<!-- Content Header (Page header) -->
@section('contentheader_title')
    Instrumentos

    @section('contentheader_description') 
      Listado de los instrumentos inscritos en el sistema
    @endsection

    @section('contentheader_level') 
      Instrumentos
    @endsection

    @section('contentheader_sublevel') 
      Listado
    @endsection




  @endsection
<!-- Main content -->
<section class="content">
    <div class="row">
      <div class="col-md-12">
                <!-- mensaje flash -->
                <div class="flash-message"></div>
                @include('flash::message')
      </div>
        <div class="col-xs-12">
            <!-- <div style="border-radius: 30px;" id="alert" class="alert-success" align="center"></div> -->
            <br>
          <div class="panel panel-default">
            <div class="panel-heading">Lista de instrumentos registrados - Total de instrumentos: {{count($instrumentos)}}

              

              <div class="btn-group pull-right" style="margin: 15px 0px 15px 15px;">
                <a href="#" data-toggle="modal" data-target="#myModal3" class="btn btn-success btn-flat" style="padding: 4px 10px; border-radius: 30px;">
                <i class="fa fa-pencil"></i> Registrar nuevo instrumento   
                </a>
              </div>
              

          </div>

          
          <div class="panel-body">
            <div class="box-body">

                
            <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Nro</th>
                <th>Nombre</th>
                <th>Marca</th>
                <th>Modelo</th>
                <th>Medidas</th>
                <th>Tipo</th>
                <th>Serial</th>
                <th>Inventario</th>
                <th>Status</th>
                <th>Opciones</th>
              </tr>
            </thead>
            <tbody>
                @foreach($instrumentos as $key)
                  <tr>
                    <td>{{$num=$num+1}}</td>
                    <td>{{$key->nombre}}</td>
                    <td>{{$key->marca}}</td>
                    <td>{{$key->modelo}}</td>
                    <td>
                      @if($key->tipo_m == 1)
                        {{$key->ancho_m}} / {{$key->largo_m}}
                      @elseif($key->tipo_m == 2)
                        {{$key->ancho_m}}"
                      @else
                        {{$key->ancho_m}} x {{$key->largo_m}} cms
                      @endif


                    </td>
                    <td>{{$key->clasif->tipo}}</td>
                    <td>{{$key->serial}}</td>
                    <td>{{$key->inventario}}</td>
                    <td>{{$key->status}}</td>
                    <td>

                    <a href="#" onclick="eliminar('{{$key->id}}')" data-toggle="modal" data-target="#myModal" class="btn btn-danger btn-flat" style="padding: 4px 10px; border-radius: 50px;"><i class="fa fa-trash"></i></a>

                    <a href="#" onclick="mostrardatos(
                        '{{$key->nombre}}',
                        '{{$key->tipo}}',
                        '{{$key->marca}}',
                        '{{$key->modelo}}',
                        '{{$key->serial}}',
                        '{{$key->inventario}}',
                        '{{$key->detalles}}',
                        '{{$key->status}}',
                        '{{$key->clasif->tipo}}',


                    )" data-toggle="modal" data-target="#myModal2" class="btn btn-default btn-flat" style="padding: 4px 10px; border-radius: 50px;"><i class="fa fa-eye"></i></a>

                  </td>
                  </tr>
                @endforeach
                
            
            </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

</div><!-- /.content-wrapper -->

<div id="myModal3" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Registrar nuevo instrumento</h4>
            </div>
            <div class="modal-body">
            
                
               {!! Form::open(['route' => ['registrarInstrumento'], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}

                  @include('admin.instrumentos.partials.create-fields')
                  <button class="btn btn-success" type="submit">Registrar instrumento</button>


            </div>
            <div class="modal-footer">
            </div>
                {!! Form::close() !!}
                
        </div>
        
    </div>
</div><!-- /.content-wrapper -->

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Eliminar instrumento</h4>
            </div>
            <div class="modal-body">
            
                ¿Está realmente seguro de querer eliminar este instrumento?
                {!! Form::open(['route' => ['deleteInstrumento'], 'method' => 'POST']) !!}
                    <input type="hidden" name="id" id="id">

                    <button class="btn btn-  btn-flat" title="Presionando este botón puede eliminar el registro" >Aceptar</button>
                {!! Form::close() !!}

                
            </div>
        </div>
        
    </div>
</div><!-- /.content-wrapper -->

<div id="myModal2"  class="modal fade" role="dialog">
  <div class="modal-dialog">
            <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Datos del instrumento</h4>
      </div>
      <div class="modal-body">               
        
        <strong>Nombre: </strong>
        <p id="n"><span></span></p>
        <br>
        <strong>Tipo: </strong>
        <p id="t"><span></span></p>
        <br>
        <strong>Marca: </strong>
        <p id="ma"><span></span></p>
        <br>
        <strong>Modelo: </strong>
        <p id="mo"><span></span></p>
        <br>
        <strong>Serial: </strong>
        <p id="se"><span></span></p>
        <br>
        <strong>Inventario: </strong>
        <p id="in"><span></span></p>
        <br>
        <strong>Detalles: </strong>
        <p id="d"><span></span></p>
        <br>
        <strong>Status: </strong>
        <p id="status"><span></span></p>
        <br>
        <strong>Clasificación: </strong>
        <p id="clasif"><span></span></p>
        <br>
        

        </div>
      <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>



<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
   <script type="text/javascript">

        function mostrardatos(nombre,tipo,marca,modelo,serial,inventario,detalles,status,clasif) 
        {
            $('#n').text(nombre);
            $('#t').text(tipo);
            $('#ma').text(marca);
            $('#mo').text(modelo);
            $('#se').text(serial);
            $('#in').text(inventario);
            $('#d').text(detalles);
            $('#status').text(status);
            $('#clasif').text(clasif);
        }

        function eliminar(id) {
          $('#id').val(id);
        }
    </script>
@endsection

@section('script')
    <script src="{{ asset('js/scripts.js') }}"></script>
@endsection
