<div class="row">
	<div class="col-md-6">
		<div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
			{!! Form::text('nombre',null,['class' => 'form-control','placeholder' => 'Nombre del instrumento', 'title' => 'Introduzca los nombres del instrumento', 'id' => 'nombre','required' => 'required']) !!}
		</div>
	</div>

	<div class="col-md-6">
		<div class="form-group{{ $errors->has('tipo') ? ' has-error' : '' }}">
			{!! Form::text('tipo',null,['class' => 'form-control','placeholder' => 'Tipo de instrumento', 'title' => 'Introduzca los nombres del instrumento', 'id' => 'tipo','required' => 'required']) !!}
		</div>
	</div>

</div>
<div class="row">
	<div class="col-md-6">
		<div class="form-group{{ $errors->has('marca') ? ' has-error' : '' }}">
			{!! Form::text('marca',null,['class' => 'form-control','placeholder' => 'marca', 'title' => 'Ingrese el marca del instrumento','id' => 'marca','required' => 'required']) !!}
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group{{ $errors->has('modelo') ? ' has-error' : '' }}">
			{!! Form::text('modelo',null,['class' => 'form-control','placeholder' => 'modelo', 'title' => 'Ingrese el modelo del instrumento','id' => 'modelo','required' => 'required']) !!}
		</div>
			
	</div>
</div>

<hr>

<div class="row">
	<div class="col-md-4">
		<div class="form-group{{ $errors->has('tipo_m') ? ' has-error' : '' }}">
			<select class="form-control select2" style="width: 100%" name="tipo_m" required id="tipo_m">
				<option value="0">Medida del instrumento</option>
				<option value="1">Fracciones</option>
				<option value="2">Pulgada</option>
				<option value="3">Cms.</option>
			</select>			
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group{{ $errors->has('ancho_m') ? ' has-error' : '' }}">
			{!! Form::number('ancho_m',null,['class' => 'form-control', 'title' => 'Ingrese el ancho del instrumento','id' => 'ancho_m']) !!}
		</div>
			
	</div>

	<div class="col-md-1">
		<div id="separa"></div>
			
	</div>

	<div class="col-md-3">
		<div class="form-group{{ $errors->has('largo_m') ? ' has-error' : '' }}">
			{!! Form::number('largo_m',null,['class' => 'form-control', 'title' => 'Ingrese el largo del instrumento','id' => 'largo_m']) !!}
		</div>
			
	</div>
	<div class="col-md-1">
		<div id="desc"></div>
			
	</div>
</div>

<hr>

<div class="row">
	<div class="col-md-4">
		<div class="form-group{{ $errors->has('id_clasif') ? ' has-error' : '' }}">
			<select class="form-control select2" style="width: 100%" name="id_clasif" required id="id_clasif">
				<option disabled>Seleccione la clasificación del instrumento</option>
				@foreach($tipos as $tipo)
					<option value="{{$tipo->id}}">{{$tipo->tipo}}</option>
				@endforeach
			</select>			
		</div>
	</div>

	<div class="col-md-4">
		<div class="form-group{{ $errors->has('serial') ? ' has-error' : '' }}">
			{!! Form::number('serial',null,['class' => 'form-control','placeholder' => 'serial', 'title' => 'Ingrese el serial del instrumento','id' => 'serial','maxlength' => '20','required' => 'required']) !!}
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group{{ $errors->has('inventario') ? ' has-error' : '' }}">
			{!! Form::number('inventario',null,['class' => 'form-control','placeholder' => 'inventario', 'title' => 'Ingrese el inventario del instrumento','id' => 'inventario', 'min' => 1, 'maxlength' => '20','required' => 'required']) !!}
		</div>
	</div>
</div>

<hr>
<div class="row">
	<div class="col-md-12">
		<div class="form-group{{ $errors->has('detalles') ? ' has-error' : '' }}">
			{!! Form::textarea('detalles',null,['class' => 'form-control','placeholder' => 'Detalles sobre el instrumento', 'title' => 'Ingrese algunos detalles que identifiquen al instrumento', 'id' => 'detalles']) !!}
		</div>
	</div>
</div>

