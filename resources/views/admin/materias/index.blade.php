@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Materias - Listado
@endsection

@section('main-content')


<!-- Content Header (Page header) -->
@section('contentheader_title')
    Materias

    @section('contentheader_description') 
      Listado de las materias inscritos en el sistema
    @endsection

    @section('contentheader_level') 
      Materias
    @endsection

    @section('contentheader_sublevel') 
      Listado
    @endsection




  @endsection
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
          <div class="flash-message"></div>
            @include('flash::message')
            <!-- <div style="border-radius: 30px;" id="alert" class="alert-success" align="center"></div> -->
            <br>
          <div class="panel panel-default">
            <div class="panel-heading">Lista de materias registrados - Total de Materias: <span id="total"></span>

              

              <div class="btn-group pull-right" style="margin: 15px 0px 15px 15px;">
                <a href="#" data-toggle="modal" data-target="#myModal3" class="btn btn-success btn-flat" style="padding: 4px 10px; border-radius: 30px;">
                <i class="fa fa-pencil"></i> Registrar nueva materia 
                </a>
              </div>
              

          </div>

          
          <div class="panel-body">
            <div class="box-body">

                
            <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Nro</th>
                <th>Materia</th>
                <th>sub-programa</th>
                <th>status</th>
                <th>Opciones</th>
              </tr>
            </thead>
            <tbody>
              @foreach($materias as $key)
                <tr>
                  <td>{{$num=$num+1}}</td>
                  <td>{{$key->materia}}</td>
                  <td>{{$key->sub_programa->sub_programa}}</td>
                  <td>{{$key->status}}</td>
                  <td></td>
                </tr>
              @endforeach()
            </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

</div><!-- /.content-wrapper -->

<div id="myModal3" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Registrar nuevo periodo</h4>
            </div>
            <div class="modal-body">
            
                
                {!! Form::open(['route' => ['registrarMateria'], 'method' => 'POST']) !!}
                
                    @include('admin.materias.partials.create-fields')

                    <button class="btn btn-success btn-flat" title="Presionando este botón puede eliminar el registro" >Aceptar</button>
                {!! Form::close() !!}

                
            </div>
        </div>
        
    </div>
</div><!-- /.content-wrapper -->



<div id="myModal4" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Editar periodo</h4>
        </div>
        <div class="modal-body">
            

                

            
        
        </div>
         </div>
       </div>
     </div>
   </div>

</div>

    <script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
   <script type="text/javascript">

       function editardatos(nombre,apellido,nacio,cedula,direccion,cod_telefono,telefono) 
        {
            $('#n').text(nombre);
            $('#a').text(apellido);
            $('#cedula').text(nacio+'.-'+cedula);
            $('#direccion').text(direccion);
            $('#telefono').text(0+cod_telefono+'-'+telefono);
        }
    </script>
@endsection

@section('script')
    <script src="{{ asset('js/scripts.js') }}"></script>
@endsection
