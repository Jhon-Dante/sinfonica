<div class="row">
	<div class="col-md-12">
		<div class="form-group{{ $errors->has('materia') ? ' has-error' : '' }}">
			{!! Form::text('materia',null,['class' => 'form-control','placeholder' => 'Nombre de la materia', 'title' => 'Introduzca la materia a impartir', 'id' => 'materia', 'required']) !!}
		</div>
	</div>
</div>
{!! Form::label('id_programa','Programas') !!}
<div class="row">
	<div class="col-md-6">
		<div class="form-group{{ $errors->has('id_modulo') ? ' has-error' : '' }}">
			<select name="id_programa" class="form-control select2" data-placeholder="Sub-programas" style="width: 100%" required id="id_programa">
				<option>Seleccione programa</option>
				@foreach($programas as $programa)
					<option value="{{$programa->id}}">{{$programa->programa}}</option>
				@endforeach()
			</select>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group{{ $errors->has('id_subPrograma') ? ' has-error' : '' }}">
			{!! Form::select('id_subPrograma',['placeholder' => 'Sub-programa'],null,['class' => 'form-control','required' => 'required', 'title' => 'Seleccione el sub-programa de la materia','style' => 'width: 100%','id' => 'id_subPrograma','disabled' => 'disabled','required' => 'required']) !!}
		</div>
	</div>
</div>


