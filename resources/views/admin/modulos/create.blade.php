@extends('layouts.app')

@section('htmlheader_title')
	estudiantes
@endsection
@section('content-wrapper')
<div class="content-wrapper">

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        @yield('contentheader_title', 'estudiantes')
        <small>Registro</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> estudiantes</a></li>
        <li class="active">Registro</li>
    </ol>
</section>
@include('alerts.requests')
<!-- Main content -->
    <section class="content spark-screen">
			<div class="row">
			<div class="col-md-12">
	         @include('flash::message')
	    </div>
					<div class="col-xs-12">
					@include('flash::message')
						<div class="panel panel-default">
							<div class="panel-heading">Registro del estudiantes  <br> Aviso: Campos con (<span style="color: red;">*</span>) son obligatorios.
 				
							</div>

							<div class="panel-body">
								
                {!! Form::open(['route' => ['admin.estudiantes.store'], 'method' => 'post', 'name' => 'estudiantes', 'id' => 'estudiantes' ]) !!}
    
					                 @include('admin.modulos.partials.create-fields')
					                
					            <div class="box-footer">
					                <button type="submit" class="btn btn-primary">Enviar</button>
					                <a class="btn btn-danger pull-right btn-flat" href="{{ url('admin/estudiantes')}}"><i class="fa fa-times"></i> Cancelar</a>
					              </div>
					              <button type="submit" class="btn btn-success">Aceptar</button>
          							<!-- /.form-group -->
          		{!! Form::close() !!} 
							</div>
						</div>
					</div>
				</div>
			
		</section>



		
</div><!-- /.content-wrapper -->
@endsection
