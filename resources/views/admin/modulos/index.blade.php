@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Módulos y núcleos - Listado
@endsection

@section('main-content')


<!-- Content Header (Page header) -->
@section('contentheader_title')
    Módulos y núcleos

    @section('contentheader_description') 
      Listado de los módulos y núcleos inscritos en el sistema
    @endsection

    @section('contentheader_level') 
      Módulos
    @endsection

    @section('contentheader_sublevel') 
      Listado
    @endsection




  @endsection
<!-- Main content -->
<section class="content">
    <div class="row">
      <div class="col-md-12">
                <!-- mensaje flash -->
                <div class="flash-message"></div>
                @include('flash::message')
      </div>
        <div class="col-xs-12">
            <!-- <div style="border-radius: 30px;" id="alert" class="alert-success" align="center"></div> -->
            <br>
          <div class="panel panel-default">
            <div class="panel-heading">Lista de módulos registrados - Total de módulos: <span id="total"></span>

              

              <div class="btn-group pull-right" style="margin: 15px 0px 15px 15px;">
                <a href="#" data-toggle="modal" data-target="#myModal3" class="btn btn-success btn-flat" style="padding: 4px 10px; border-radius: 30px;">
                <i class="fa fa-pencil"></i> Registrar nuevo módulo   
                </a>
              </div>
              

          </div>

          
          <div class="panel-body">
            <div class="box-body">

                
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Nro</th>
                  <th>Rif</th>
                  <th>Nombre</th>
                  <th>Tipo</th>
                  <th>Ocupación</th>
                  <th>Parroquia</th>
                  <th>Ciudad</th>
                  <th>Status</th>
                  <th>Foto</th>
                  <th>Opciones</th>
                </tr>
              </thead>
              <tbody>
                  
                  @foreach($modulos as $key)

                    <tr>
                      <td>{{$num=$num+1}}</td>
                      <td>J.- {{$key->rif}}</td>
                      <td>{{$key->nombre}}</td>
                      <td>{{$key->tipo}}</td>
                      <td>{{$key->ocupacion}}</td>
                      <td>{{$key->parroquia->parroquia}}</td>
                      <td>{{$key->ciudad->ciudad}}</td>
                      <td>{{$key->status}}</td>
                      <td><img class="img-responsive" src="{{ asset('/storage/app/logo1.jpg') }}" alt="" style="border-radius: 50px"></td>
                      <td>
                        
                          <a href="#" onclick="mostrardatos(
                                        '{{$key->nombre}}',
                                        '{{$key->rif}}',
                                        '{{$key->direccion}}',
                                        '{{$key->tipo}}',
                                        '{{$key->ocupacion}}',
                                        '{{$key->direccion}}',
                                        '{{$key->cod_telefono}}',
                                        '{{$key->telefono}}')" 

                                        data-toggle="modal" data-target="#myModal2" class="btn btn-default btn-flat" style="padding: 4px 10px; border-radius: 50px;">
                          <i class="fa fa-eye"></i>
                          </a>

                          <a href="#" onclick="nuevoNucleo(
                                        '{{$key->nombre}}',
                                        '{{$key->rif}}',
                                        '{{$key->parroquia->parroquia}}',
                                        '{{$key->ciudad->ciudad}}',
                                        '{{$key->parroquia->municipio->municipio}}',
                                        '{{$key->parroquia->municipio->estado->estado}}'
                                        )" 

                                        data-toggle="modal" data-target="#myModal4" class="btn btn-warning btn-flat" style="padding: 4px 10px; border-radius: 50px;">
                          <i class="fa fa-eye"></i>
                          </a>

                      
                      </td>
                    </tr>


                  @endforeach
                  
              
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
</section>

<div id="myModal2"  class="modal fade" role="dialog">
  <div class="modal-dialog">
            <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Datos del Núcleo</h4>
      </div>
      <div class="modal-body">               
        <strong>Nombre </strong>
        <p id="n"><span></span></p>
        <br>
        <strong>Rif </strong>
        <p id="a"><span></span></p>
        <br>
        <strong>Direccion </strong> 
        <p id="cedula"><span></span></p>
        <br>
        <strong>Tipo: </strong>
        <p id="direccion"><span></span></p>
        <br>
        <strong>Ocupacion: </strong> 
        <p id="telefono"><span></span></p>
        <br>
        <strong>¿Núcleo principal? </strong> 
        <p id="telefono"><span></span></p>
        <br>
        <strong>Status: </strong> 
        <p id="telefono"><span></span></p>
        <br>
        <strong>Parroquia: </strong> 
        <p id="telefono"><span></span></p>
        <br>
        <strong>Ciudad: </strong> 
        <p id="telefono"><span></span></p>
      </div>
      <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div id="myModal3" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Registrar nuevo núcleo</h4>
            </div>
            <div class="modal-body">
            
                {!! Form::open(['route' => ['registrarModulo'], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                  
                  @include('admin.modulos.partials.create-fields')

              </div>
              <div class="box-footer">
                  
                      <button type="submit" class="btn btn-success">Aceptar</button>
              </div>
            {!! Form::close() !!}
                

                
            </div>
        </div>
        
    </div>
</div><!-- /.content-wrapper -->


<div id="myModal4"  class="modal fade" role="dialog">
  <div class="modal-dialog">
            <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">¿Especificar nuevo núcleo?</h4>
      </div>
      <div class="modal-body">     
        <div class="row">
          <div class="col-md-4">
            <strong>Nombre del módulo </strong>
            <p id="nombre"><span></span></p>
          </div>

          <div class="col-md-4">
            <strong>Rif: </strong>
            <p id="rif"><span></span></p>
          </div>
          
          <div class="col-md-4">
            <strong>Parroquia: </strong> 
            <p id="parroquia"><span></span></p>
          </div>
        </div>

        <div class="row">
          <div class="col-md-4">
            <strong>Ciudad </strong>
            <p id="ciudad"><span></span></p>
          </div>

          <div class="col-md-4">
            <strong>Municipio: </strong>
            <p id="municipio"><span></span></p>
          </div>
          
          <div class="col-md-4">
            <strong>Estado: </strong> 
            <p id="estado"><span></span></p>
          </div>

          <hr>
          <br>

        </div>           
        <div class="row">
          <div class="col-md-10">
            <label>Candidatos</label>
            <select class="select2" style="width: 100%">
              <option disabled="">Personal Activo</option>
              <option disabled="">Coordinadores</option>
            </select>
          </div>
          <div class="col-md-2">
            <br>
            <a href="#" data-toggle="modal" data-target="#myModal5" class="btn btn-success btn-flat" style="padding: 4px 10px; border-radius: 50px;">
                          <i class="fa fa-eye"></i></a>
          </div>
        </div>
            
          
        
        
        
          
      </div>
      <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div id="myModal5" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Nuevo coordinador</h4>
        </div>
        <div class="modal-body">
            

                

            
        
        </div>
         </div>
       </div>
     </div>
   </div>

</div>
<div id="myModal4" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Editar Cliente</h4>
        </div>
        <div class="modal-body">
            

                

            
        
        </div>
         </div>
       </div>
     </div>
   </div>

</div>


    <script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
   <script type="text/javascript">

        function mostrardatos(nombre,apellido,nacio,cedula,direccion,cod_telefono,telefono) 
        {
          // alert(nombre+apellido);
            $('#n').text(nombre);
            $('#a').text(apellido);
            $('#cedula').text(nacio+'.-'+cedula);
            $('#direccion').text(direccion);
            $('#telefono').text(0+cod_telefono+'-'+telefono);
        }

        function nuevoNucleo(nombre,rif,parroquia,ciudad,municipio,estado) 
        {
          // alert(nombre+apellido);
            $('#nombre').text(nombre);
            $('#rif').text(rif);
            $('#parroquia').text(parroquia);
            $('#ciudad').text(ciudad);
            $('#municipio').text(municipio);
            $('#estado').text(estado);
        }

        function editardatos(nombre,apellido,nacio,cedula,direccion,cod_telefono,telefono) 
        {
            $('#n').text(nombre);
            $('#a').text(apellido);
            $('#cedula').text(nacio+'.-'+cedula);
            $('#direccion').text(direccion);
            $('#telefono').text(0+cod_telefono+'-'+telefono);
        }
    </script>
@endsection

@section('script')
    <script src="{{ asset('js/scripts.js') }}"></script>
@endsection
