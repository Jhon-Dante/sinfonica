<div class="row">
	<div>
		
	</div>
</div>

<div class="row">
	<div class="col-md-12">
			{!! Form::label('foto_add','Foto de la institución')!!}
			{!! Form::file('foto_add',null,['class' => 'form-control','placeholder' => 'Nombre del núcleo', 'title' => 'Introduzca el nombre de la institución a representar como núcleo', 'id' => 'foto_add','required' => 'required']) !!}
	</div>	
</div>

<hr>

<div class="row">
	<div class="col-md-6">
		<div class="form-group{{ $errors->has('nombre_add') ? ' has-error' : '' }}">
			{!! Form::text('nombre_add',null,['class' => 'form-control','placeholder' => 'Nombre del núcleo', 'title' => 'Introduzca el nombre de la institución a representar como núcleo', 'id' => 'nombre_add','required' => 'required']) !!}
		</div>
	</div>

	<div class="col-md-6">
		<div class="form-group{{ $errors->has('rif_add') ? ' has-error' : '' }}">
			{!! Form::number('rif_add',null,['class' => 'form-control','placeholder' => 'RIF', 'title' => 'Ingrese la rif del nuevo núcleo','min' => '6','maxLength' => '8', 'id' => 'rif_add','required' => 'required']) !!}
		</div>
	</div>
	
</div>


<div class="row">
	<div class="col-md-6">
		<div class="form-group{{ $errors->has('id_estado') ? ' has-error' : '' }}">
			<select id="id_estado" name="id_estado" class="form-control select2" required="required" title="Seleccione el estado donde se registrará el nuevo núcleo" style="width: 100%;">
				@foreach($estados as $estado)
						<option value="{{$estado->id}}">{{$estado->estado}}</option>
				@endforeach
			</select>
		</div>
	</div>

	<div class="col-md-6">
		<div class="form-group{{ $errors->has('id_minicipio') ? ' has-error' : '' }}">
			{!! Form::select('id_municipio',['placeholder' => 'Municipio'],null,['class' => 'form-control select2','required' => 'required', 'title' => 'Seleccione el municipio donde se encuentra localizado el nuevo núcelo','style' => 'width: 100%','id' => 'id_municipio','disabled' => 'disabled','required' => 'required']) !!}
		</div>
	</div>
</div>
<br>

<div class="row">
	<div class="col-md-6">
		<div class="form-group{{ $errors->has('id_ciudad') ? ' has-error' : '' }}">
			{!! Form::select('id_ciudad',['placeholder' => 'Ciudad'],null,['class' => 'form-control select2','required' => 'required', 'title' => 'Seleccione la ciudad donde se encuentra localizado el nuevo núcelo','style' => 'width: 100%','id' => 'id_ciudad','disabled' => 'disabled','required' => 'required']) !!}
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group{{ $errors->has('id_parroquia') ? ' has-error' : '' }}">
			{!! Form::select('id_parroquia',['placeholder' => 'Parroquia'],null,['class' => 'form-control select2','required' => 'required', 'title' => 'Seleccione el municipio donde se encuentra localizado el nuevo núcelo','style' => 'width: 100%','id' => 'id_parroquia','disabled' => 'disabled','required' => 'required']) !!}
		</div>
	</div>
</div>
<br>

<!-- <div class="row">
	<div class="col-md-4">
		<div class="form-group{{ $errors->has('fecha_nacimiento') ? ' has-error' : '' }}">

			{!! Form::label('fecha_nacimiento','Fecha de nacimiento') !!}
			{!! Form::date('fecha_nacimiento',null,['class' => 'form-control', 'title' => 'Ingrese la fecha de nacimiento del personal','required' => 'required']) !!}
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group{{ $errors->has('genero') ? ' has-error' : '' }}">
			{!! Form::label('genero','Sexo') !!}
			{!! Form::select('genero', ['M' => 'M','F' => 'F'],null, ['class' => 'form-control', 'title' => 'Seleccione la nacionalidad del personal','required' => 'required']) !!}
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group{{ $errors->has('edo_civil') ? ' has-error' : '' }}">
		{!! Form::label('edo_cvil','Estado Civil') !!}
		{!! Form::select('edo_civil', ['Soltero(a)' => 'Soltero(a)', 'Casado(a)' => 'Casado(a)', 'Concuvino(a)' => 'Concuvino(a)','Viudo(a)' => 'Viudo(a)'],null, ['class' => 'form-control', 'title' => 'Seleccione el estado civil del personal','required' => 'required']) !!}
		</div>
	</div>
</div> -->
<div class="row">
	<div class="col-md-6">
		<div class="form-group{{ $errors->has('tipo_add') ? ' has-error' : '' }}">
			<select id="tipo_add" name="tipo" class="form-control select2" required="required" title="Seleccione el tipo de institución a registrar" style="width: 100%;" id="tipo_add">
				<option value="" disabled>Tipo de institución</option>
				<option value="publica">Institución pública</option>
				<option value="privada">Institución privada</option>
			</select>
		</div>
	</div>

	<div class="col-md-6">
		<div class="form-group{{ $errors->has('ocupacion_add') ? ' has-error' : '' }}">
			{!! Form::text('ocupacion_add',null,['placeholder' => 'Ocupación','class' => 'form-control','required' => 'required', 'title' => 'Especifique a que se dedica la institución','style' => 'width: 100%','id' => 'ocupacion_add','required' => 'required']) !!}
		</div>
	</div>
</div>
<br>

<div class="row">
	<div class="col-md-12">
		<div class="form-group{{ $errors->has('direccion_add') ? ' has-error' : '' }}">
			{!! Form::textarea('direccion_add',null,['class' => 'form-control','placeholder' => 'Dirección de la institución', 'title' => 'Ingrese la dirección de la institución', 'id' => 'direccion_add','required' => 'required']) !!}
		</div>
	</div>
</div>

<div class="row">	
		<div class="col-md-4">
			<div class="form-group{{ $errors->has('codigo_telefono_add') ? ' has-error' : '' }}">
	
				{!! Form::label('codigo_telefono_add','código de habitación') !!}
				{!! Form::select('codigo_telefono_add',[
				'0244' => '0244', 
				'0424' => '0424',
				'0416' => '0416',
				'0426' => '0426'
				],null,['class' => 'form-control', 'title' => 'Seleccione la nacionalidad del personal', 'maxlength','7','id' => 'cod_telefono_add','required' => 'required']) !!}
			</div>
		</div>
		<div class="col-md-8">
			<div class="form-group{{ $errors->has('telefono_add') ? ' has-error' : '' }}">
	
				{!! Form::label('telefono_add','Teléfono de habitación') !!}
				{!! Form::number('telefono_add',null,['class' => 'form-control','placeholder' => 'Ej: 2455643', 'title' => 'Ingrese el teléfono móvil del personal','title' => 'Ingrese la cedula del personal','min' => '7','maxLength' => '7','oninput' => 'javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);','id' => 'telefono_add','required' => 'required']) !!}
			</div>
		</div>
</div>
<div class="row">	
		<div class="col-md-12">
			<div class="form-group{{ $errors->has('email_add') ? ' has-error' : '' }}">
			{!! Form::email('email_add',null,['class' => 'form-control','placeholder' => 'correo', 'title' => 'Ingrese el primer y segundo apellido del personal', 'id'=> 'email_add','required' => 'required']) !!}
			</div>
		</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="form-group{{ $errors->has('detalles_add') ? ' has-error' : '' }}">
			{!! Form::textarea('detalles_add',null,['class' => 'form-control','placeholder' => '¿Algun detalle u observaciones acerca de la institución?', 'title' => 'Ingrese la dirección de la institución', 'id' => 'detalles_add','required' => 'required']) !!}
		</div>
	</div>
</div>