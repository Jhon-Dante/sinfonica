<table>
	<tr>
		<td>
			<img src="..\public\img\logo 2.png" style="width: 200px; height: 200px;">
		</td>
		<td>
			<img src="..\public\img\logo imagen.jpg" style="width: 500px; height: 200px;">
		</td>
	</tr>
</table>
<br><br>
<h3 align="center">CONSTANCIA DE ESTUDIOS</h3>

<br><br><br><br>
<p align="justify">&nbsp;&nbsp;&nbsp;&nbsp; El suscrito Director de La Orquesta Sinfónica "Simón Bolivar", núcleo La Victoria. Prof. Alfredo Ascanio, C.I. V-00000000, por medio de la presente hace  constar, que el(a) Estudiante, <strong>{{$inscripcion->datoBasico->nombres}}, {{$inscripcion->datoBasico->apellidos}}</strong> portador(a) de la Cédula de Identidad/Passport Nro. <strong>{{$inscripcion->datoBasico->nacio}}.-{{$inscripcion->datoBasico->cedula}}</strong>, nacido(a) en la fecha   {{$inscripcion->datoBasico->fecha_nac}}. Cursa los estudios correspondientes al programa {{$inscripcion->sub_programa->programa->programa}}, en el subprograma {{$inscripcion->sub_programa->sub_programa}}, durante el Período Escolar {{$periodo->periodo}}.
<br><br>
</p>
<p>Constancia que se expíde por todo el año {{$año}}</p>
<br><br><br><br><br><br><br><br><br><br><br>
<p align="center">___________________________________________</p>

<p align="center">Alfredo Ascanio.</p>
<p align="center">DIRECTOR</p>