<table>
	<tr>
		<td>
			<img src="..\public\img\logo 2.png" style="width: 200px; height: 200px;">
		</td>
		<td>
			<img src="..\public\img\logo imagen.jpg" style="width: 500px; height: 200px;">
		</td>
	</tr>
</table>
<br><br>
<h3 align="center">CONSTANCIA</h3>

<br><br><br><br>
<p align="justify">&nbsp;&nbsp;&nbsp;&nbsp; Quien suscribe actual Director del Sistema de Orquestas y Coros del Estado Aragua Núcleos La Victoria Ciudadano  ALFREDO ASCANIO, titular de la Cédula de Identidad N 9.690.044 por medio de la presente, hace constar que el (la) joven:  <strong>{{$inscripcion->datoBasico->nombres}} {{$inscripcion->datoBasico->apellidos}}</strong>, titular de la Cédula de Identidad N.- <strong>{{$inscripcion->datoBasico->cedula}}</strong> Es alumno(a) regular de esta Institución, solicita se le conceda permiso para el día(s) <u>           {{$dia}}           </u> a partir de las <u>           {{$hora}}           </u> . Estará participando <u>           {{$motivo}}           </u>

Agradeciendo su atención y colaboración a la presente.

	Constancia que se expide de parte interesada en la Ciudad de la Victoria a los <?php echo Date('d'); ?> días del mes <?php echo Date('m'); ?> del <?php echo Date('Y'); ?>

<br><br>
</p>
<p>Constancia que se expíde por todo el año {{$año}}</p>
<br><br><br><br><br><br><br><br><br><br><br>
<p align="center">___________________________________________</p>

<p align="center">Alfredo Ascanio.</p>
<p align="center">DIRECTOR</p>