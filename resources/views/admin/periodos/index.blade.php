@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Periodos - Listado
@endsection

@section('main-content')


<!-- Content Header (Page header) -->
@section('contentheader_title')
    Periodos

    @section('contentheader_description') 
      Listado de los Periodos inscritos en el sistema
    @endsection

    @section('contentheader_level') 
      Periodos
    @endsection

    @section('contentheader_sublevel') 
      Listado
    @endsection




  @endsection
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
          @if ($errors->any())
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif
          <div class="flash-message"></div>
            @include('flash::message')
            <!-- <div style="border-radius: 30px;" id="alert" class="alert-success" align="center"></div> -->
            <br>
          <div class="panel panel-default">
            <div class="panel-heading">Lista de Periodos registrados - Total de Periodos: <span id="total"></span>

              

              <div class="btn-group pull-right" style="margin: 15px 0px 15px 15px;">
                <a href="#" data-toggle="modal" data-target="#myModal3" class="btn btn-success btn-flat" style="padding: 4px 10px; border-radius: 30px;">
                <i class="fa fa-pencil"></i> Registrar nuevo periodo   
                </a>
              </div>
              

          </div>

          
          <div class="panel-body">
            <div class="box-body">

                
            <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Nro</th>
                <th>Periodo</th>
                <th>Creado el</th>
                <th>Modificado el</th>
                <th>Status</th>
                <th>Opciones</th>
              </tr>
            </thead>
            <tbody>
              @foreach($periodos as $key)

                <tr>
                  
                  <td>{{$num=$num+1}}</td>
                  <td>{{$key->periodo}}</td>
                  <td>{{$key->created_at}}</td>
                  <td>{{$key->updated_at}}</td>
                  <td>{{$key->status}}</td>
                  <td></td>
                </tr>

              @endforeach
            </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

</div><!-- /.content-wrapper -->

<div id="myModal3" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Registrar nuevo periodo</h4>
            </div>
            <div class="modal-body">
            
                
                {!! Form::open(['route' => ['storePeriodo'], 'method' => 'POST']) !!}
                
                    @include('admin.periodos.partials.create-fields')

                    <button class="btn btn-success btn-flat" data-toggle="modal" data-target="#myModal" title="Presionando este botón puede eliminar el registro" ><i class="fa fa-user-plus"></i></button>
                {!! Form::close() !!}

                
            </div>
        </div>
        
    </div>
</div><!-- /.content-wrapper -->



<div id="myModal4" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Editar periodo</h4>
        </div>
        <div class="modal-body">
            

                

            
        
        </div>
         </div>
       </div>
     </div>
   </div>

</div>

    <script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
   <script type="text/javascript">

       function editardatos(nombre,apellido,nacio,cedula,direccion,cod_telefono,telefono) 
        {
            $('#n').text(nombre);
            $('#a').text(apellido);
            $('#cedula').text(nacio+'.-'+cedula);
            $('#direccion').text(direccion);
            $('#telefono').text(0+cod_telefono+'-'+telefono);
        }
    </script>
@endsection

@section('script')
    <script src="{{ asset('js/scripts.js') }}"></script>
@endsection
