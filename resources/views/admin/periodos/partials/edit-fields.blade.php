<div class="row">
	<div class="col-md-6">
		<div class="form-group{{ $errors->has('nombres') ? ' has-error' : '' }}">
			{!! Form::text('nombre_edit',null,['class' => 'form-control','placeholder' => 'Nombres', 'title' => 'Phjgh', 'id' => 'nombre_edit', 'required']) !!}
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group{{ $errors->has('apellidos') ? ' has-error' : '' }}">
			{!! Form::text('apellido_edit',null,['class' => 'form-control','placeholder' => 'Apellidos', 'title' => 'Ingrese el primer y segundo apellido del personal', 'id'=> 'apellido_edit']) !!}
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-2">
		<div class="form-group{{ $errors->has('nacionalidad') ? ' has-error' : '' }}">
			{!! Form::select('nacio_edit', ['V'=>'V', 'E'=>'E'],null, ['class' => 'form-control', 'title' => 'Seleccione el estado civil del personal', 'id' => 'nacio_edit']) !!}
		</div>
	</div>
	<div class="col-md-10">
		<div class="form-group{{ $errors->has('cedula') ? ' has-error' : '' }}">
			{!! Form::number('cedula_edit',null,['class' => 'form-control','placeholder' => 'Cédula', 'title' => 'Ingrese la cedula del personal','min' => '6','maxLength' => '8','oninput' => 'javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);', 'id' => 'cedula_edit']) !!}
		</div>
	</div>
</div>
<!-- <div class="row">
	<div class="col-md-4">
		<div class="form-group{{ $errors->has('fecha_nacimiento') ? ' has-error' : '' }}">

			{!! Form::label('fecha_nacimiento','Fecha de nacimiento') !!}
			{!! Form::date('fecha_nacimiento',null,['class' => 'form-control', 'title' => 'Ingrese la fecha de nacimiento del personal']) !!}
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group{{ $errors->has('genero') ? ' has-error' : '' }}">
			{!! Form::label('genero','Sexo') !!}
			{!! Form::select('genero', ['M' => 'M','F' => 'F'],null, ['class' => 'form-control', 'title' => 'Seleccione la nacionalidad del personal']) !!}
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group{{ $errors->has('edo_civil') ? ' has-error' : '' }}">
		{!! Form::label('edo_cvil','Estado Civil') !!}
		{!! Form::select('edo_civil', ['Soltero(a)' => 'Soltero(a)', 'Casado(a)' => 'Casado(a)', 'Concuvino(a)' => 'Concuvino(a)','Viudo(a)' => 'Viudo(a)'],null, ['class' => 'form-control', 'title' => 'Seleccione el estado civil del personal']) !!}
		</div>
	</div>
</div> -->
<div class="form-group{{ $errors->has('direccion') ? ' has-error' : '' }}">
	{!! Form::textarea('direccion_edit',null,['class' => 'form-control','placeholder' => 'Dirección del cliente', 'title' => 'Ingrese la dirección del personal', 'id' => 'direccion_edit']) !!}
</div>

<div class="row">	
		<div class="col-md-4">
			<div class="form-group{{ $errors->has('codigo_hab') ? ' has-error' : '' }}">
	
				{!! Form::label('codigo_telefono_edit','codigo de habitación') !!}
				{!! Form::select('codigo_telefono_edit',[
				'0244' => '0244', 
				'0424' => '0424',
				'0416' => '0416',
				'0426' => '0426'
				],null,['class' => 'form-control', 'title' => 'Seleccione la nacionalidad del personal', 'maxlength','7','id' => 'cod_telefono_edit']) !!}
			</div>
		</div>
		<div class="col-md-8">
			<div class="form-group{{ $errors->has('telefono') ? ' has-error' : '' }}">
	
				{!! Form::label('telefono_edit','Teléfono de habitación') !!}
				{!! Form::number('telefono_edit',null,['class' => 'form-control','placeholder' => 'Ej: 2455643', 'title' => 'Ingrese el teléfono móvil del personal','title' => 'Ingrese la cedula del personal','min' => '7','maxLength' => '7','oninput' => 'javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);','id' => 'telefono_edit']) !!}
			</div>
		</div>
</div>