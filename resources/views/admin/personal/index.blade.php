@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Personal
@endsection

@section('main-content')


<!-- Content Header (Page header) -->
@section('contentheader_title')
    Personal

    @section('contentheader_description') 
      Personal
    @endsection

    @section('contentheader_level') 
      Personal
    @endsection

    @section('contentheader_sublevel') 
      Personal
    @endsection




  @endsection
<!-- Main content -->
<section class="content">
    <div class="row">
      <div class="col-md-12">
                <!-- mensaje flash -->
                <div class="flash-message"></div>
                @include('flash::message')
      </div>
        <div class="col-xs-12">
            <!-- <div style="border-radius: 30px;" id="alert" class="alert-success" align="center"></div> -->
            <br>
          <div class="panel panel-default">
            <div class="panel-heading">Lista del personal registrado

              

              <div class="btn-group pull-right" style="margin: 15px 0px 15px 15px;">
                <a href="#" data-toggle="modal" data-target="#myModal3" class="btn btn-success btn-flat" style="padding: 4px 10px; border-radius: 30px;">
                <i class="fa fa-pencil"></i> Registrar nuevo personal   
                </a>
              </div>
              

          </div>

          
          <div class="panel-body">
            <div class="box-body">

                
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Nro</th>
                  <th>Nombre</th>
                  <th>Cédula/Passport</th>
                  <th>RIF</th>
                  <th>Email</th>
                  <th>Cargo</th>
                  <th>Status</th>
                  <th>Opciones</th>
                </tr>
              </thead>
              <tbody>
                  
                  @foreach($personal as $key)

                    <tr>
                      <td>{{$num=$num+1}}</td>
                      <td>{{$key->nombre}} {{$key->apellidos}}</td>
                      <td>{{$key->nacio}}.- {{$key->cedula}}</td>
                      <td>{{$key->rif}}</td>
                      <td>{{$key->email}}</td>
                      <td>{{$key->cargo->cargo}}</td>
                      <td>

                        @if($key->status == 'si')
                          <a href="{{ route('statusPersonal', [$key->id]) }}">Si</a>
                        @else
                          <a href="{{ route('statusPersonal', [$key->id]) }}">No</a>
                        @endif
                          
                      <td>
                        
                          <a href="#" onclick="mostrardatos(
                                        '{{$key->nombre}} {{$key->apellidos}}',
                                        '{{$key->rif}}',
                                        '{{$key->direccion}}',
                                        '{{$key->tipo}}',
                                        '{{$key->ocupacion}}',
                                        '{{$key->direccion}}',
                                        '{{$key->cod_telefono}}',
                                        '{{$key->telefono}}')" 

                                        data-toggle="modal" data-target="#myModal2" class="btn btn-default btn-flat" style="padding: 4px 10px; border-radius: 50px;">
                          <i class="fa fa-eye"></i>
                          </a>

                         
                    <a href="#" onclick="eliminar('{{$key->id}}')" data-toggle="modal" data-target="#myModal" class="btn btn-danger btn-flat" style="padding: 4px 10px; border-radius: 50px;"><i class="fa fa-trash"></i>

                      
                      </td>
                    </tr>


                  @endforeach
                  
              
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
</section>

<div id="myModal2"  class="modal fade" role="dialog">
  <div class="modal-dialog">
            <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Datos del Personal</h4>
      </div>
      <div class="modal-body">               
        <strong>Nombre </strong>
        <p id="n"><span></span></p>
        <br>
        <strong>Rif </strong>
        <p id="a"><span></span></p>
        <br>
        <strong>Direccion </strong> 
        <p id="cedula"><span></span></p>
        <br>
        <strong>Tipo: </strong>
        <p id="direccion"><span></span></p>
        <br>
        <strong>Ocupacion: </strong> 
        <p id="telefono"><span></span></p>
        <br>
        <strong>¿Núcleo principal? </strong> 
        <p id="telefono"><span></span></p>
        <br>
        <strong>Status: </strong> 
        <p id="telefono"><span></span></p>
        <br>
        <strong>Parroquia: </strong> 
        <p id="telefono"><span></span></p>
        <br>
        <strong>Ciudad: </strong> 
        <p id="telefono"><span></span></p>
      </div>
      <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div id="myModal3" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Registrar nuevo núcleo</h4>
            </div>
            <div class="modal-body">
            
                {!! Form::open(['route' => ['registrarPersonal'], 'method' => 'POST']) !!}
                  
                  @include('admin.personal.partials.create-fields')

              </div>
              <div class="box-footer">
                  
                      <button type="submit" class="btn btn-success">Aceptar</button>
              </div>
            {!! Form::close() !!}
                

                
            </div>
        </div>
        
    </div>
</div><!-- /.content-wrapper -->


<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Eliminar programa</h4>
            </div>
            <div class="modal-body">
            
                ¿Está realmente seguro de querer eliminar este personal?
                {!! Form::open(['route' => ['eliminarPersonal'], 'method' => 'POST']) !!}
                    <input type="hidden" name="id" id="id">

                    <button class="btn btn-  btn-flat" title="Presionando este botón puede eliminar el registro" >Aceptar</button>
                {!! Form::close() !!}

                
            </div>
        </div>
        
    </div>
</div><!-- /.content-wrapper -->



    <script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
   <script type="text/javascript">

        function mostrardatos(nombre,apellido,nacio,cedula,direccion,cod_telefono,telefono) 
        {
          // alert(nombre+apellido);
            $('#n').text(nombre);
            $('#a').text(apellido);
            $('#cedula').text(nacio+'.-'+cedula);
            $('#direccion').text(direccion);
            $('#telefono').text(0+cod_telefono+'-'+telefono);
        }

        function eliminar(id) {
          $('#id').val(id);
        }
    </script>
@endsection

@section('script')
    <script src="{{ asset('js/scripts.js') }}"></script>
@endsection
