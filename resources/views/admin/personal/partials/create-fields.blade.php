<!-- div class="row">
	<div class="col-md-12">
			{!! Form::label('foto_add','Foto de la institución')!!}
			{!! Form::file('foto_add',null,['class' => 'form-control','placeholder' => 'Nombre del núcleo', 'title' => 'Introduzca el nombre de la institución a representar como núcleo', 'id' => 'foto_add','required' => 'required']) !!}
	</div>	
</div> -->

<div class="row">
	<div class="col-md-6">
		<div class="form-group{{ $errors->has('nombre_add') ? ' has-error' : '' }}">
			{!! Form::text('nombre',null,['class' => 'form-control','placeholder' => 'Nombres', 'title' => 'Introduzca el nombre del nuevo personal', 'id' => 'nombre_add','required' => 'required']) !!}
		</div>
	</div>

	<div class="col-md-6">
		<div class="form-group{{ $errors->has('apellidos') ? ' has-error' : '' }}">
			{!! Form::TEXT('apellidos',null,['class' => 'form-control','placeholder' => 'Apellidos', 'title' => 'Ingrese el apellido del nuev personal', 'id' => 'apellidos','required' => 'required']) !!}
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-2">
		<div class="form-group{{ $errors->has('nacio') ? ' has-error' : '' }}">
			<select name="nacio" class="form-control select2" style="width: 100%">
				<option disabled>Nacionalidad</option>
				<option value="V">V</option>
				<option value="E">E</option>
			</select>
		</div>
	</div>

	<div class="col-md-5">
		<div class="form-group{{ $errors->has('cedula') ? ' has-error' : '' }}">
			{!! Form::number('cedula',null,['class' => 'form-control','placeholder' => 'Cédula/Passport', 'title' => 'Ingrese la cedula del nuev personal','min' => '6','maxLength' => '8', 'id' => 'cedula','required' => 'required']) !!}
		</div>
	</div>
	<div class="col-md-5">
		<div class="form-group{{ $errors->has('rif') ? ' has-error' : '' }}">
			{!! Form::number('rif',null,['class' => 'form-control','placeholder' => 'RIF', 'title' => 'Ingrese el rif del nuev personal','min' => '6','maxLength' => '8', 'id' => 'rif','required' => 'required']) !!}
		</div>
	</div>
</div>

<hr>

<div class="row">
	<div class="col-md-6">
		<div class="form-group{{ $errors->has('id_estado') ? ' has-error' : '' }}">
			<select id="id_estado" name="id_estado" class="form-control select2" required="required" title="Seleccione el estado donde se registrará el nuevo núcleo" style="width: 100%;">
				@foreach($estados as $estado)
						<option value="{{$estado->id}}">{{$estado->estado}}</option>
				@endforeach
			</select>
		</div>
	</div>

	<div class="col-md-6">
		<div class="form-group{{ $errors->has('id_minicipio') ? ' has-error' : '' }}">
			{!! Form::select('id_municipio',['placeholder' => 'Municipio'],null,['class' => 'form-control select2','required' => 'required', 'title' => 'Seleccione el municipio donde se encuentra localizado el nuevo núcelo','style' => 'width: 100%','id' => 'id_municipio','disabled' => 'disabled','required' => 'required']) !!}
		</div>
	</div>
</div>
<br>

<div class="row">
	<div class="col-md-6">
		<div class="form-group{{ $errors->has('id_ciudad') ? ' has-error' : '' }}">
			{!! Form::select('id_ciudad',['placeholder' => 'Ciudad'],null,['class' => 'form-control select2','required' => 'required', 'title' => 'Seleccione la ciudad donde se encuentra localizado el nuevo núcelo','style' => 'width: 100%','id' => 'id_ciudad','disabled' => 'disabled','required' => 'required']) !!}
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group{{ $errors->has('id_parroquia') ? ' has-error' : '' }}">
			{!! Form::select('id_parroquia',['placeholder' => 'Parroquia'],null,['class' => 'form-control select2','required' => 'required', 'title' => 'Seleccione el municipio donde se encuentra localizado el nuevo núcelo','style' => 'width: 100%','id' => 'id_parroquia','disabled' => 'disabled','required' => 'required']) !!}
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="form-group{{ $errors->has('direccion') ? ' has-error' : '' }}">
			{!! Form::textarea('direccion',null,['class' => 'form-control','placeholder' => 'Dirección del personal: Casa, Apt, nro de avenida...', 'title' => 'Ingrese la dirección de la institución', 'id' => 'direccion','required' => 'required']) !!}
		</div>
	</div>
</div>

<hr>

<div class="row">
	
	<div class="col-md-6">
		<div class="form-group{{ $errors->has('edo_civil') ? ' has-error' : '' }}">
		{!! Form::label('estado_c','Estado Civil') !!}
		{!! Form::select('estado_c', ['Soltero(a)' => 'Soltero(a)', 'Casado(a)' => 'Casado(a)', 'Concuvino(a)' => 'Concuvino(a)','Viudo(a)' => 'Viudo(a)'],null, ['class' => 'form-control select2', 'style' => 'width:100%;','title' => 'Seleccione el estado civil del personal','required' => 'required']) !!}
		</div>
	</div>
	<div class="col-md-6">
		
		<div class="form-group{{ $errors->has('genero') ? ' has-error' : '' }}">
			{!! Form::label('genero','Sexo') !!}
			{!! Form::select('genero', ['M' => 'M','F' => 'F'],null, ['class' => 'form-control', 'title' => 'Seleccione la nacionalidad del personal','required' => 'required']) !!}
		</div>
	</div>
</div>
<div class="row">
	
	<div class="col-md-6">
		<div class="form-group{{ $errors->has('fecha_n') ? ' has-error' : '' }}">
		{!! Form::label('fecha_n','Fecha de nacimiento') !!}
		{!! Form::date('fecha_n',null, ['class' => 'form-control','title' => 'Seleccione la fecha de nacimiento del personal','required' => 'required']) !!}
		</div>
	</div>
	<div class="col-md-6">
		
		<div class="form-group{{ $errors->has('lugar_n') ? ' has-error' : '' }}">
			{!! Form::label('lugar_n','Lugar de nacimiento') !!}
			{!! Form::text('lugar_n',null, ['class' => 'form-control', 'title' => 'Especifique la fecha de nacimiento del personal','required' => 'required']) !!}
		</div>
	</div>
</div>
<hr>



<div class="row">	
		<div class="col-md-4">
			<div class="form-group{{ $errors->has('cod_telefono') ? ' has-error' : '' }}">
	
				{!! Form::label('cod_telefono','Código de teléfono') !!}
				{!! Form::select('cod_telefono',[
				'0244' => '0244', 
				'0424' => '0424',
				'0416' => '0416',
				'0426' => '0426'
				],null,['class' => 'form-control select2', 'title' => 'Seleccione el código de telefono del personal', 'maxlength','7','id' => 'cod_telefono','required' => 'required']) !!}
			</div>
		</div>
		<div class="col-md-8">
			<div class="form-group{{ $errors->has('telefono') ? ' has-error' : '' }}">
	
				{!! Form::label('telefono','Teléfono') !!}
				{!! Form::number('telefono',null,['class' => 'form-control','placeholder' => 'Ej: 2455643', 'title' => 'Ingrese el teléfono móvil del personal','min' => '7','maxLength' => '7','oninput' => 'javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);','id' => 'telefono','required' => 'required']) !!}
			</div>
		</div>
</div>

<div class="row">	
		<div class="col-md-4">
			<div class="form-group{{ $errors->has('codigo_telefono_add') ? ' has-error' : '' }}">
	
				{!! Form::label('cod_telefono_hab','Código de habitación') !!}
				{!! Form::select('cod_telefono_hab',[
				'0244' => '0244', 
				'0424' => '0424',
				'0416' => '0416',
				'0426' => '0426'
				],null,['class' => 'form-control select2', 'title' => 'Seleccione el código de teléfono de habitación del personal', 'maxlength','7','id' => 'cod_telefono_hab','required' => 'required']) !!}
			</div>
		</div>
		<div class="col-md-8">
			<div class="form-group{{ $errors->has('telefono_hab') ? ' has-error' : '' }}">
	
				{!! Form::label('telefono_hab','Teléfono de habitación') !!}
				{!! Form::number('telefono_hab',null,['class' => 'form-control','placeholder' => 'Ej: 2455643', 'title' => 'Ingrese el teléfono de casa del personal','min' => '7','maxLength' => '7','oninput' => 'javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);','id' => 'telefono_hab','required' => 'required']) !!}
			</div>
		</div>
</div>
<div class="row">	
		<div class="col-md-12">
			<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
			{!! Form::email('email',null,['class' => 'form-control','placeholder' => 'correo', 'title' => 'Ingrese el primer y segundo apellido del personal', 'id'=> 'email','required' => 'required']) !!}
			</div>
		</div>
</div>
<hr>
<div class="row">
	<div class="col-md-12">
		<div class="form-group{{ $errors->has('cuenta_v') ? ' has-error' : '' }}">
			{!! Form::text('cuenta_v',null,['class' => 'form-control','placeholder' => '¿Tiene cuenta del banco de Venezuela?', 'title' => 'Ingrese la cuenta del Banco de Venezuela', 'id' => 'cuenta_v','required' => 'required']) !!}
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="form-group{{ $errors->has('cuenta_p') ? ' has-error' : '' }}">
			{!! Form::text('cuenta_p',null,['class' => 'form-control','placeholder' => '¿Tiene cuenta del banco de Provincial?', 'title' => 'Ingrese la cuenta del Banco de Provincial', 'id' => 'cuenta_p','required' => 'required']) !!}
		</div>
	</div>
</div>

<hr>

<div class="row">
	<div class="col-md-6	">
		<div class="form-group{{ $errors->has('id_cargo') ? ' has-error' : '' }}">
			<select name="id_cargo" id="id_cargo" class="form-control select2" style="width: 100%">
				<option disabled>Seleccione el cargo que desempeñará el personal</option>
				@foreach($cargos as $key2)
					<option value="{{$key2->id}}">{{$key2->cargo}}</option>
				@endforeach
			</select>
		</div>
	</div>

	<div class="col-md-6">
		<div class="form-group{{ $errors->has('createUser') ? 'has-error' : '' }}">
			<div class="checkbox">
			    <label>
			     	<input type="checkbox" name="createUser" value="si" id="createUser">
			     	¿Este empleado tendrá inicio de sesión?
			    </label>
			 </div>

		</div>

	</div>
</div>