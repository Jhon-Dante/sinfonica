<div class="row">
	<div class="col-md-12">
		<div class="form-group{{ $errors->has('programa') ? ' has-error' : '' }}">
			{!! Form::text('programa',null,['class' => 'form-control','placeholder' => 'Nombre del programa', 'title' => 'Introduzca el programa de la institución a representar como núcleo', 'id' => 'programa', 'required']) !!}
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="form-group{{ $errors->has('id_modulo') ? ' has-error' : '' }}">
			{!! Form::label('id_modulo','Módulos') !!}
			<select name="id_modulo[]" multiple class="form-control" data-placeholder="Módulos" required>
				@foreach($modulos as $modulo)
					<option value="{{$modulo->id}}">{{$modulo->nombre}}-{{$modulo->rif}}</option>
				@endforeach()
			</select>
		</div>
	</div>
</div>


