@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Representantes - Listado
@endsection

@section('main-content')


<!-- Content Header (Page header) -->
@section('contentheader_title')
    Representantes

    @section('contentheader_description') 
      Listado de los representantes inscritos en el sistema
    @endsection

    @section('contentheader_level') 
      Representantes
    @endsection

    @section('contentheader_sublevel') 
      Listado
    @endsection




@endsection
<!-- Main content -->
<section class="content">
    <div class="row">
      <div class="col-md-12">
        @if ($errors->any())
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif
                <!-- mensaje flash -->
        <div class="flash-message"></div>
        @include('flash::message')
      </div>
        <div class="col-xs-12">
            <!-- <div style="border-radius: 30px;" id="alert" class="alert-success" align="center"></div> -->
            <br>
          <div class="panel panel-default">
            <div class="panel-heading">Lista de representantes registrados - Total de representantes: <span id="total"></span>

              

              <div class="btn-group pull-right" style="margin: 15px 0px 15px 15px;">
                <a href="#" data-toggle="modal" data-target="#myModal3" class="btn btn-success btn-flat" style="padding: 4px 10px; border-radius: 30px;">
                <i class="fa fa-pencil"></i> Registrar nuevo representante   
                </a>
              </div>
              

          </div>

          
          <div class="panel-body">
            <div class="box-body">

                
            <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Nro</th>
                <th>Nombre</th>
                <th>Cédula</th>
                <th>Correo</th>
                <th>Telefono</th>
                <th>Representado(s)</th>
                <th>Módulo</th>
                <th>Foto</th>
                <th>Opciones</th>
              </tr>
            </thead>
            <tbody>
                @foreach($representantes as $key)
                  <tr>
                    <td>{{$num=$num+1}}</td>
                    <td>{{$key->nombres}} {{$key->apellidos}}</td>
                    <td>{{$key->nacio}}.-{{$key->cedula}}</td>
                    <td>{{$key->email}}</td>
                    <td>{{$key->cod_cel}}{{$key->telefono_cel}}</td>
                    <td></td>
                    <td>{{$key->modulo->nombre}}</td>
                    <td><img src="{{ url($key->foto) }}" style="width: 100px; height: 100px; border-radius: 100px;"></td>
                    <td></td>
                  </tr>
                @endforeach
                
            
            </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

</div><!-- /.content-wrapper -->

<div id="myModal3" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Registrar nuevo representante</h4>
            </div>
            <div class="modal-body">
            
                
                {!! Form::open(['route' => ['registrarRepresentante'], 'method' => 'POST', 'enctype' => 'multipart/form-data', 'files' => 'true']) !!}

                  {{ csrf_field() }}
                  @include('admin.representantes.partials.create-fields')


                
            </div>
            <div class="modal-footer">
              <button class="btn btn-success" type="submit">Registrar representante</button>
            </div>
                {!! Form::close() !!}
        </div>
        
    </div>
</div>
<!-- /.content-wrapper -->


<div id="myModal2"  class="modal fade" role="dialog">
  <div class="modal-dialog">
            <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Datos del representante</h4>
      </div>
      <div class="modal-body">               
        <strong>Nombres: </strong>
        <p id="n"><span></span></p>
        <br>
        <strong>Apellidos: </strong>
        <p id="a"><span></span></p>
        <br>
        <strong>Cédula: </strong> 
        <p id="cedula"><span></span></p>
        <br>
        <strong>Direccion: </strong>
        <p id="direccion"><span></span></p>
        <br>
        <strong>telefono: </strong> 
        <p id="telefono"><span></span></p>
        </div>
      <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<div id="myModal4" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Editar representante</h4>
        </div>
        <div class="modal-body">
            

                @include('admin.datosBasicos.partials.edit-fields')

            
        
        </div>
         </div>
       </div>
     </div>
   </div>

</div>

    <script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
   <script type="text/javascript">

        function mostrardatos(nombre,apellido,nacio,cedula,direccion,cod_telefono,telefono) 
        {
          // alert(nombre+apellido);
            $('#n').text(nombre);
            $('#a').text(apellido);
            $('#cedula').text(nacio+'.-'+cedula);
            $('#direccion').text(direccion);
            $('#telefono').text(0+cod_telefono+'-'+telefono);
        }

        function editardatos(nombre,apellido,nacio,cedula,direccion,cod_telefono,telefono) 
        {
            $('#n').text(nombre);
            $('#a').text(apellido);
            $('#cedula').text(nacio+'.-'+cedula);
            $('#direccion').text(direccion);
            $('#telefono').text(0+cod_telefono+'-'+telefono);
        }
    </script>
@endsection

@section('script')
    <script src="{{ asset('js/scripts.js') }}"></script>
@endsection
