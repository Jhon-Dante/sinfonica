<div class="row">
	<div class="col-md-12">
			{!! Form::label('foto_add','Foto del representante')!!}
			{!! Form::file('foto_add',null,['class' => 'form-control','placeholder' => 'Nombre del núcleo', 'title' => 'Introduzca el nombre de la institución a representar como núcleo', 'id' => 'foto_add']) !!}
	</div>	
</div>

<hr>

<div class="row">
	<div class="col-md-6">
		<div class="form-group{{ $errors->has('nombre_add') ? ' has-error' : '' }}">
			{!! Form::text('nombres_add',null,['class' => 'form-control','placeholder' => 'Nombres del representante', 'title' => 'Introduzca el nombre de la institución a representar como núcleo', 'id' => 'nombres_add']) !!}
		</div>
	</div>

	<div class="col-md-6">
		<div class="form-group{{ $errors->has('rif_add') ? ' has-error' : '' }}">
			{!! Form::text('apellidos_add',null,['class' => 'form-control','placeholder' => 'Apellidos del representante', 'title' => 'Ingrese la rif del nuevo núcleo','id' => 'apellidos_add']) !!}
		</div>
	</div>
</div>

{!! Form::label('nacio_add','Cédula/passaporte del representante') !!}
<div class="row">
	<div class="col-md-4">
		<div class="form-group{{ $errors->has('nombre_add') ? ' has-error' : '' }}">
			{!! Form::select('nacio_add',['V' => 'V', 'E' => 'E'],null,['class' => 'form-control','title' => 'Ingrese la cedula del personal','min' => '6','maxLength' => '7','oninput' => 'javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);','id' => 'telefono_opc_add']) !!}
		</div>
	</div>

	<div class="col-md-8">
		<div class="form-group{{ $errors->has('cedula_add') ? ' has-error' : '' }}">
			{!! Form::number('cedula_add',null,['class' => 'form-control','placeholder' => 'Cédula', 'title' => 'Ingrese la cédula del personal','min' => '7','maxLength' => '7','oninput' => 'javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);','id' => 'cedula_add']) !!}
		</div>
	</div>
</div>

<hr>
<div class="row">
	<div class="col-md-2">
		<div class="form-group">
            <label>Masculino
              <input type="radio" name="genero_add" class="minimal" checked value="m">
            </label>
         </div>
    </div>
    <div class="col-md-2">
    	<div class="form-group">
            <label>Femenino
              <input type="radio" name="genero_add" class="minimal" value="f">
            </label>
    	</div>
	</div>
	<div class="col-md-1">
    	<div class="form-group">
            <label>Otros
              <input type="radio" name="genero_add" class="minimal" value="otros">
            </label>
    	</div>
	</div>

	<div class="col-md-7">
		{!! Form::text('profesion_add',null,['class' => 'form-control', 'placeholder' => 'Profesión','id']) !!}
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="form-group{{ $errors->has('direccion_add') ? ' has-error' : '' }}">
			{!! Form::textarea('direccion_add',null,['class' => 'form-control','placeholder' => 'Dirección del representante', 'title' => 'Ingrese la dirección de la institución', 'id' => 'direccion_add']) !!}
		</div>
	</div>
</div>

<hr>

{!! Form::label('cod_hab','Teléfono de habitación') !!}
<div class="row">
	<div class="col-md-4">
		<div class="form-group{{ $errors->has('cod_hab_add') ? ' has-error' : '' }}">

			{!! Form::select('cod_hab_add',[
			'0244' => '0244', 
			'0424' => '0424',
			'0416' => '0416',
			'0426' => '0426'
			],null,['class' => 'form-control', 'title' => 'Seleccione la nacionalidad del personal', 'maxlength','7','id' => 'cod_hab_add']) !!}
		</div>
	</div>
	<div class="col-md-8">
		<div class="form-group{{ $errors->has('telefono_hab_add') ? ' has-error' : '' }}">
			{!! Form::number('telefono_hab_add',null,['class' => 'form-control','placeholder' => 'Teléfono', 'title' => 'Ingrese el teléfono móvil del personal','title' => 'Ingrese la cedula del personal','min' => '7','maxLength' => '7','oninput' => 'javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);','id' => 'telefono_hab_add']) !!}
		</div>
	</div>
</div>

{!! Form::label('cod_hab','Teléfono celular') !!}
<div class="row">
	<div class="col-md-4">
		<div class="form-group{{ $errors->has('cod_cel_add') ? ' has-error' : '' }}">

			{!! Form::select('cod_cel_add',[
			'0244' => '0244', 
			'0424' => '0424',
			'0416' => '0416',
			'0426' => '0426'
			],null,['class' => 'form-control', 'title' => 'Seleccione la nacionalidad del personal', 'maxlength','7','id' => 'cod_cel_add']) !!}
		</div>
	</div>
	<div class="col-md-8">
		<div class="form-group{{ $errors->has('telefono_cel_add') ? ' has-error' : '' }}">
			{!! Form::number('telefono_cel_add',null,['class' => 'form-control','placeholder' => 'Teléfono', 'title' => 'Ingrese el teléfono móvil del personal','title' => 'Ingrese la cedula del personal','min' => '7','maxLength' => '7','oninput' => 'javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);','id' => 'telefono_cel_add']) !!}
		</div>
	</div>
</div>

{!! Form::label('cod_hab','Teléfono opcional') !!}
<div class="row">
	<div class="col-md-4">
		<div class="form-group{{ $errors->has('cod_opc_add') ? ' has-error' : '' }}">

			{!! Form::select('cod_opc_add',[
			'0244' => '0244', 
			'0424' => '0424',
			'0416' => '0416',
			'0426' => '0426'
			],null,['class' => 'form-control', 'title' => 'Seleccione la nacionalidad del personal', 'maxlength','7','id' => 'cod_opc_add']) !!}
		</div>
	</div>
	<div class="col-md-8">
		<div class="form-group{{ $errors->has('telefono_opc_add') ? ' has-error' : '' }}">
			{!! Form::number('telefono_opc_add',null,['class' => 'form-control','placeholder' => 'Teléfono', 'title' => 'Ingrese el teléfono móvil del personal','title' => 'Ingrese la cedula del personal','min' => '7','maxLength' => '7','oninput' => 'javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);','id' => 'telefono_opc_add']) !!}
		</div>
	</div>
</div>

<hr>
<div class="row">
	<div class="col-md-6">
		<div class="form-group{{ $errors->has('lugar_tra_add') ? ' has-error' : '' }}">
			{!! Form::text('lugar_tra_add',null,['class' => 'form-control','placeholder' => 'Lugar de trabajo', 'title' => 'Introduzca el nombre de la institución a representar como núcleo', 'id' => 'lugar_tra_add']) !!}
		</div>
	</div>

	<div class="col-md-6">
		<div class="form-group{{ $errors->has('cargo_tra_add') ? ' has-error' : '' }}">
			{!! Form::text('cargo_tra_add',null,['class' => 'form-control','placeholder' => 'Cargo en el trabajo', 'title' => 'Ingrese la rif del nuevo núcleo','id' => 'cargo_tra_add']) !!}
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="form-group{{ $errors->has('email_add') ? ' has-error' : '' }}">
			{!! Form::email('email_add',null,['class' => 'form-control','placeholder' => 'Correo del representante', 'title' => 'Introduzca el nombre de la institución a representar como núcleo', 'id' => 'email_add']) !!}
		</div>
	</div>
</div>