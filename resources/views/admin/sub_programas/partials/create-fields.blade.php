<div class="row">
	<div class="col-md-12">
		<div class="form-group{{ $errors->has('sub_programa') ? ' has-error' : '' }}">
			{!! Form::text('sub_programa',null,['class' => 'form-control','placeholder' => 'Nombre del sub-programa', 'title' => 'Introduzca el sub-programa', 'id' => 'sub_programa', 'required']) !!}
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="form-group{{ $errors->has('id_modulo') ? ' has-error' : '' }}">
			{!! Form::label('id_programa','Programa') !!}
			<select name="id_programa" class="form-control" data-placeholder="Programas" required>
				@foreach($programas as $programa)
					<option value="{{$programa->id}}">{{$programa->programa}}</option>
				@endforeach()
			</select>
		</div>
	</div>
</div>


