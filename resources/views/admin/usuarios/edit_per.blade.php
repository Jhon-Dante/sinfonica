@extends('adminlte::layouts.app')

@section('htmlheader_title')
    Usuarios - Editar permisos de usuarios
@endsection

@section('main-content')


<!-- Content Header (Page header) -->
@section('contentheader_title')
    Permisos de usuarios

    @section('contentheader_description') 
      Listado de los permisos que tiene los usuarios a las diferentes funcionalidades del sistema
    @endsection

    @section('contentheader_level') 
      Usuarios
    @endsection

    @section('contentheader_sublevel') 
      Editar permisos de usuarios
    @endsection




  @endsection
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
          <div class="flash-message"></div>
            @include('flash::message')
            <!-- <div style="border-radius: 30px;" id="alert" class="alert-success" align="center"></div> -->
            <br>
          <div class="panel panel-default">
            <div class="panel-heading">Lista de Usuarios registrados - Total de Usuarios: <span id="total"></span>

              
              

          </div>

        {!! Form::open(['route' => ['editPer'], 'method' => 'POST']) !!}
        <div class="panel-body">
            <div class="box-body" style="overflow: scroll;">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Nro</th>
                        <th>Usuario</th>
                        <th>Tipo de usuario</th>
                        <th>Pre/registrar estudiante</th>
                        <th>Ver listado de estudiantes</th>
                        <th>Editar datos de estudiantes</th>
                        <th>Eliminar estudiantes</th>
                        <th>Generar constancia de estudios</th>
                        <th>Generar constancia de audiciones</th>
                        <th>Generar permisos de estudiantes</th>
                        <!-- <th>Listar Representantes</th>
                        <th>Registrar representante</th>
                        <th>Editar representante</th>
                        <th>Pagar mensualidades</th>
                        <th>editar montos</th>
                        <th>Editar montos de matrícula</th>
                        <th>Editar momentos de preescolar</th>
                        <th>Editar calificaciones de básica</th>
                        <th>Editar calificaciones de Media General</th>
                        <th>Editar Notas finales</th>
                        <th>Generar horarios</th>
                        <th>Listar Personal</th>
                        <th>Registrar personal</th>
                        <th>Editar datos de personal</th>
                        <th>Asignar carga cadémica</th>
                        <th>Asignar profesores guías</th>
                        <th>Listar profesores Guías</th>
                        <th>Listar usuarios</th>
                        <th>Editar datos de usuarios</th>
                        <th>Listar asignaturas</th>
                        <th>Registrar asignaturas</th>
                        <th>Editar asignaturas</th>
                        <th>Eliminar asignaturas</th>
                        <th>Listar auditoría</th>
                        <th>Listar aulas</th>
                        <th>Registrar aulas</th>
                        <th>Editar aulas</th>
                        <th>Eliminar aulas</th>
                        <th>Listar cargos</th>
                        <th>Registrar cargos</th>
                        <th>Editar cargos</th>
                        <th>Eliminar Cargos</th>
                        <th>Listar Periodos</th>
                        <th>Crear Periodos</th>
                        <th>Editar periodos</th>
                        <th>Eliminar Periodos</th>
                        <th>Activar-desactivar periodos</th>
                        <th>Restaurar BD</th>
                        <th>Listar secciones</th>
                        <th>Registrar usuarios</th>
                        <th>Editar secciones</th>
                        <th>Eliminar Secciones</th> -->
                        
                      </tr>
                    </thead>
                    <tbody>

                        
	                        @foreach($usuarios as $usuario)
	                        
		                            <tr>
		                                <td><input type="hidden" name="id[]" value="{{$usuario->id}}">{{$num=$num+1}}</td>
		                                <td>{{$usuario->name}}</td>
		                                <td>{{$usuario->tipo_user}}</td>
		                                <td>
		                                	@if($usuario->pre_re == 'Si')
		                                		<div class="form-group" style="color:green;">
													<div class="checkbox">
													    <label>
													     	<input checked type="checkbox" name="pre_re[]" value="Si">
													     	Autorizado
													    </label>
													</div>
												</div>
		                                	@else
		                                		<div class="form-group" style="color:red;">
													<div class="checkbox">
													    <label>
													     	<input type="checkbox" name="pre_re[]" value="Si">
													     	No autorizado
													    </label>
													 </div>
												</div>
		                                	@endif
		                                </td>
		                                <td>
		                                	@if($usuario->list_estu == 'Si')
		                                		<div class="form-group" style="color:green;">
													<div class="checkbox">
													    <label>
													     	<input checked type="checkbox" name="list_estu[]" value="Si">
													     	Autorizado
													    </label>
													</div>
												</div>
		                                	@else
		                                		<div class="form-group" style="color:red;">
													<div class="checkbox">
													    <label>
													     	<input type="checkbox" name="list_estu[]" value="Si">
													     	No autorizado
													    </label>
													 </div>
												</div>
		                                	@endif
		                                </td>
		                                <td>
		                                	@if($usuario->edit_estu == 'Si')
		                                		<div class="form-group" style="color:green;">
													<div class="checkbox">
													    <label>
													     	<input checked type="checkbox" name="edit_estu[]" value="Si">
													     	Autorizado
													    </label>
													</div>
												</div>
		                                	@else
		                                		<div class="form-group" style="color:red;">
													<div class="checkbox">
													    <label>
													     	<input type="checkbox" name="edit_estu[]" value="Si">
													     	No autorizado
													    </label>
													 </div>
												</div>
		                                	@endif
		                                </td>
		                                <td>
		                                	@if($usuario->eli_estu == 'Si')
		                                		<div class="form-group" style="color:green;">
													<div class="checkbox">
													    <label>
													     	<input checked type="checkbox" name="eli_estu[]" value="Si">
													     	Autorizado
													    </label>
													</div>
												</div>
		                                	@else
		                                		<div class="form-group" style="color:red;">
													<div class="checkbox">
													    <label>
													     	<input type="checkbox" name="eli_estu[]" value="Si">
													     	No autorizado
													    </label>
													 </div>
												</div>
		                                	@endif
		                                </td>
		                                <td>
		                                	@if($usuario->const_estu == 'Si')
		                                		<div class="form-group" style="color:green;">
													<div class="checkbox">
													    <label>
													     	<input checked type="checkbox" name="const_estu[]" value="Si">
													     	Autorizado
													    </label>
													</div>
												</div>
		                                	@else
		                                		<div class="form-group" style="color:red;">
													<div class="checkbox">
													    <label>
													     	<input type="checkbox" name="const_estu[]" value="Si">
													     	No autorizado
													    </label>
													 </div>
												</div>
		                                	@endif
		                                </td>
		                                <td>
		                                	@if($usuario->cer_estu == 'Si')
		                                		<div class="form-group" style="color:green;">
													<div class="checkbox">
													    <label>
													     	<input checked type="checkbox" name="cer_estu[]" value="Si">
													     	Autorizado
													    </label>
													</div>
												</div>
		                                	@else
		                                		<div class="form-group" style="color:red;">
													<div class="checkbox">
													    <label>
													     	<input type="checkbox" name="cer_estu[]" value="Si">
													     	No autorizado
													    </label>
													 </div>
												</div>
		                                	@endif
		                                </td>
		                                <td>
		                                	@if($usuario->titulob_estu == 'Si')
		                                		<div class="form-group" style="color:green;">
													<div class="checkbox">
													    <label>
													     	<input checked type="checkbox" name="titulob_estu[]" value="Si">
													     	Autorizado
													    </label>
													</div>
												</div>
		                                	@else
		                                		<div class="form-group" style="color:red;">
													<div class="checkbox">
													    <label>
													     	<input type="checkbox" name="titulob_estu[]" value="Si">
													     	No autorizado
													    </label>
													 </div>
												</div>
		                                	@endif
		                                </td>
		                                <!-- <td><a href="#">{{$usuario->list_repre}}</a></td>
		                                <td><a href="#">{{$usuario->create_repre}}</a></td>
		                                <td><a href="#">{{$usuario->edit_repre}}</a></td>
		                                <td><a href="#">{{$usuario->pag_mensu}}</a></td>
		                                <td><a href="#">{{$usuario->edit_montos}}</a></td>
		                                <td><a href="#">{{$usuario->edit_monto_m}}</a></td>
		                                <td><a href="#">{{$usuario->edit_cali_pre}}</a></td>
		                                <td><a href="#">{{$usuario->edit_cali_basic}}</a></td>
		                                <td><a href="#">{{$usuario->edit_cali_media}}</a></td>
		                                <td><a href="#">{{$usuario->edit_notas_final}}</a></td>
		                                <td><a href="#">{{$usuario->gen_horario}}</a></td>
		                                <td><a href="#">{{$usuario->list_perso}}</a></td>
		                                <td><a href="#">{{$usuario->create_perso}}</a></td>
		                                <td><a href="#">{{$usuario->edit_perso}}</a></td>
		                                <td><a href="#">{{$usuario->asig_car_aca}}</a></td>
		                                <td><a href="#">{{$usuario->asig_guia}}</a></td>
		                                <td><a href="#">{{$usuario->list_guia}}</a></td>
		                                <td><a href="#">{{$usuario->list_user}}</a></td>
		                                <td><a href="#">{{$usuario->list_edit}}</a></td>
		                                <td><a href="#">{{$usuario->list_asig}}</a></td>
		                                <td><a href="#">{{$usuario->create_asig}}</a></td>
		                                <td><a href="#">{{$usuario->edit_asig}}</a></td>
		                                <td><a href="#">{{$usuario->elim_asig}}</a></td>
		                                <td><a href="#">{{$usuario->list_auditoria}}</a></td>
		                                <td><a href="#">{{$usuario->list_aula}}</a></td>
		                                <td><a href="#">{{$usuario->create_aula}}</a></td>
		                                <td><a href="#">{{$usuario->edit_aula}}</a></td>
		                                <td><a href="#">{{$usuario->elim_aula}}</a></td>
		                                <td><a href="#">{{$usuario->list_cargo}}</a></td>
		                                <td><a href="#">{{$usuario->create_cargo}}</a></td>
		                                <td><a href="#">{{$usuario->edit_cargo}}</a></td>
		                                <td><a href="#">{{$usuario->elim_cargo}}</a></td>
		                                <td><a href="#">{{$usuario->list_periodo}}</a></td>
		                                <td><a href="#">{{$usuario->create_periodo}}</a></td>
		                                <td><a href="#">{{$usuario->edit_periodo}}</a></td>
		                                <td><a href="#">{{$usuario->elim_periodo}}</a></td>
		                                <td><a href="#">{{$usuario->res_BD}}</a></td>
		                                <td><a href="#">{{$usuario->list_seccion}}</a></td>
		                                <td><a href="#">{{$usuario->create_seccion}}</a></td>
		                                <td><a href="#">{{$usuario->edit_seccion}}</a></td>
		                                <td><a href="#">{{$usuario->elim_seccion}}</a></td> -->
		                            </tr>
	                        @endforeach
                        
                    </tbody>
                </table>
          	</div>
      	</div>
      	<div class="panel-footer" align="right">
        	<button class="btn btn-primary  btn-flat" title="Presionando este botón puede eliminar el registro" >Guardar</button>
        </div>
        {!! Form::close() !!}
    </div>
  </div>
</section>

</div><!-- /.content-wrapper -->


<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Eliminar cargo</h4>
            </div>
            <div class="modal-body">
            
                ¿Está realmente seguro de querer eliminar este cargo seleccionado?
                {!! Form::open(['route' => ['eliminarCargo'], 'method' => 'POST']) !!}
                    <input type="hidden" name="id" id="id">

                    <button class="btn btn-  btn-flat" title="Presionando este botón puede eliminar el registro" >Aceptar</button>
                {!! Form::close() !!}

                
            </div>
        </div>
        
    </div>
</div><!-- /.content-wrapper -->

<div id="myModal3" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Ingrese la contraseña</h4>
        </div>
        {!! Form::open(['route' => ['permisosUser'], 'method' => 'POST']) !!}
        <div class="modal-body">
          <p>Ingrese su contraseña de administrador</p>
        
          <div class="form-group has-feedback">
            <input type="password" required="required" class="form-control" placeholder="{{ trans('adminlte_lang::message.password') }}" name="password"/>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Aceptar</button>
        </div>
          {!! Form::close() !!}
         </div>
       </div>
     </div>
   </div>

</div><!-- /.content-wrapper -->
</div>



<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
   <script type="text/javascript">

        function eliminar(id) {
          $('#id').val(id);
        }
    </script>
@endsection

@section('script')
    <script src="{{ asset('js/scripts.js') }}"></script>
@endsection
