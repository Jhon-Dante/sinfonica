<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        <!-- <a href="https://github.com/acacha/adminlte-laravel"></a><b>admin-lte-laravel</b></a>. {{ trans('adminlte_lang::message.descriptionpackage') }} -->
        Todos los derechos reservados a la Institución
    </div>
    <!-- Default to the left -->
    <strong>VicSinphony &copy; 2018 .</strong> {{ trans('adminlte_lang::message.createdby') }} <a href="http://github.com/Jhon-Dante">Javier Buffone</a>. {{ trans('adminlte_lang::message.seecode') }} 
</footer>
