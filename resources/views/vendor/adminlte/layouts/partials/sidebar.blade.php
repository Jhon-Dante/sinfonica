<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ Gravatar::get($user->email) }}" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p style="overflow: hidden;text-overflow: ellipsis;max-width: 160px;" data-toggle="tooltip" title="{{ Auth::user()->name }}">{{ Auth::user()->name }}</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('adminlte_lang::message.online') }}</a>
                </div>
            </div>
        @endif

        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="{{ trans('adminlte_lang::message.search') }}..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">{{ trans('adminlte_lang::message.header') }}</li>
            <!-- Optionally, you can add icons to the links -->
            <li class="active"><a href="{{ url('home') }}"><i class='fa fa-link'></i> <span>{{ trans('adminlte_lang::message.home') }}</span></a></li>
            @if(Auth::user()->pre_re == 'Si')
                <li><a href="{{ url('admin/datosBasicos/create') }}"><i class='fa fa-book'></i> <span>Inscripción</span></a></li>
            @endif


            
            <!-- Estusiantes -->
            @if(Auth::user()->list_estu == 'Si')
                <li class="treeview">
                    <a href="#"><i class='fa fa-child'></i><span>Estudiantes</span><i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{ url('admin/datosBasicos') }}"><i class='fa fa-user'></i>Listado</a></li>
                        <li><a href="{{ url('admin/representantes') }}"><i class='fa fa-users'></i>Representantes</a></li>
                    </ul>
                </li>
            @endif

             <li class="treeview">
                <a href="#"><i class='fa fa-object-ungroup'></i><span>Inventario</span><i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('admin/instrumentos') }}"><i class='fa fa-object-group'></i>Instrumentos</a></li>
                    <li><a href="#"><i class='fa fa-object-group'></i>Mobiliario</a></li>
                </ul>
            </li>


             <li><a href="{{ url('admin/modulos') }}"><i class='fa fa-link'></i> <span>Módulos y Núcleos</span></a></li>

            <!-- Configuracion -->
            <li class="treeview">
                <a href="#"><i class='fa fa-cog'></i><span>Configuracion</span><i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('admin/usuarios') }}"><i class='fa fa-user'></i>Usuarios</a></li>
                    <li><a href="{{ url('admin/personal') }}"><i class='fa fa-user'></i>Personal</a></li>
                    <li><a href="{{ url('admin/cargos') }}"><i class='fa fa-users'></i>Cargos</a></li>
                    
                    <hr>
                    <li><a href="{{ url('admin/programas') }}"><i class='fa fa-users'></i>Programas</a></li>
                    <li><a href="{{ url('admin/subprogramas') }}"><i class='fa fa-users'></i>Sub-programas</a></li>
                    <li><a href="{{ url('admin/materias') }}"><i class='fa fa-users'></i>Materias</a></li>
                    <hr>
                    <li><a href="#"><i class='fa fa-database'></i>Respaldo</a></li>
                    <li><a href="{{ url('admin/auditoria')}}"><i class='fa fa-user-secret'></i>Auditoria</a></li>
                    <hr>
                    <li><a href="{{ url('admin/periodos') }}"><i class='fa fa-calendar-minus-o'></i>Periodos</a></li>
                    <li><a href="{{ url('/logout') }}" id="logout"
                                       onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        {{ trans('adminlte_lang::message.signout') }}
                                    </a></li>
                </ul>
            </li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
