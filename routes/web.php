<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
	
	Route::get('/home', 'HomeController@index')->name('home');

	Route::resource('/datosBasicos','DatosBasicosController');
	Route::resource('/representantes','RepresentantesController');
	Route::resource('/modulos','ModulosController');
	Route::resource('/periodos','PeriodosController');
	Route::resource('/programas','ProgramasController');
	Route::resource('/subprogramas','SubProgramasController');
	Route::resource('/materias','MateriasController');
	Route::resource('/personal','DatosBasicosPersonalController');
	Route::resource('/cargos','CargosController');
	Route::resource('/auditoria','AuditoriaController');
	Route::resource('/usuarios','UserController');


	//Núcleos
	Route::post('/registrar_modulo','ModulosController@store')->name('registrarModulo');


	//PERIODOS
	Route::post('/registrar_periodo','PeriodosController@storePeriodo')->name('storePeriodo');
	Route::get('/periodos/buscar','PeriodosController@listarPeriodo')->name('listarPeriodo');


	//DIRECCIONES
	Route::get('/subprograma/{id}/buscar','SubProgramasController@subprograma')->name('subprograma');
	Route::get('/municipio/{id}/buscar','MunicipiosController@buscarMunicipio')->name('buscar_municipio');
	Route::get('/ciudad/{id}/buscar','CiudadesController@buscarCiudad')->name('buscar_ciudad');
	Route::get('/parroquia/{id}/buscar','ParroquiasController@buscarParroquia')->name('buscar_parroquia');
	Route::get('/materias/{id}/buscar','MateriasController@buscarMaterial')->name('buscar_materia');

	//Representantes

	Route::post('/registrar_representante','RepresentantesController@store')->name('registrarRepresentante');


	//Estudiantes
	Route::post('/preinscripcion','DatosBasicosController@store')->name('prinscripcion');
	Route::post('/inscripcionCreate','DatosBasicosController@inscripcionCreate')->name('inscripcionCreate');
	Route::post('/reinscripcion','DatosBasicosController@reinscripcion')->name('reinscripcion');
	Route::get('/constanciaI/{id}','DatosBasicosController@constanciaI')->name('constanciaI');
	Route::get('/constanciaAudicion/{id}','DatosBasicosController@constanciaAudicion')->name('constanciaAudicion');
	Route::get('/constanciaPermiso','DatosBasicosController@constanciaPermiso')->name('constanciaPermiso');


	//INVENTARIO
		//Instrumentos
		Route::resource('/instrumentos','InstrumentosController');
		Route::post('/registrarInstrumento','InstrumentosController@store')->name('registrarInstrumento');
		Route::post('/instrumentos/eliminar','InstrumentosController@delete')->name('deleteInstrumento');

	

	//CONFIGURACION

		//USUARIOS

		Route::get('/usuario/{id}/status','UserController@status')->name('statusUser');
		Route::post('/usuario/permisos','UserController@permisos')->name('permisosUser');
		Route::post('/usuarios/editPer','UserController@editPer')->name('editPer');

		//PERSONAL
		Route::post('/registrar_personal','DatosBasicosPersonalController@store')->name('registrarPersonal');
		Route::post('/eliminar_personal','DatosBasicosPersonalController@destroy')->name('eliminarPersonal');
		Route::get('/personal/{id}/status','DatosBasicosPersonalController@status')->name('statusPersonal');
		//CARGOS
		Route::post('/registrar_cargo','CargosController@store')->name('registrarCargo');
		Route::post('/eliminar_cargo','CargosController@destroy')->name('eliminarCargo');
		Route::get('/cargo/{id}/status','CargosController@status')->name('statusCargo');
		//PROGRAMAS
		Route::post('/registrar_programa','ProgramasController@store')->name('registrarPrograma');
		Route::post('/eliminar_programa','ProgramasController@delete')->name('eliminarPrograma');
		Route::get('/programa/{id}/status','ProgramasController@statusPrograma')->name('statusPrograma');

		//SUBPROGRAMAS
		Route::post('/registrar_subprograma','SubProgramasController@store')->name('registrarSubprograma');

		//MATERIAS
		Route::post('/registrar_materias','MateriasController@store')->name('registrarMateria');



	
    //    Route::get('/link1', function ()    {
//        // Uses Auth Middleware
//    });

    //Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.
    #adminlte_routes
});
